/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSDBUSREGISTER_H
#define SYSDBUSREGISTER_H

#include <QCoreApplication>
#include <QProcess>
#include <QDir>
#include <QDBusMessage>
#include <QtDBus/QDBusInterface>
#include <QDBusConnection>
#include <QDBusReply>
#include "powerconfig.h"

#include <stdio.h>
class SysdbusRegister : public QObject
{
    Q_OBJECT

    Q_CLASSINFO("D-Bus Interface", "org.ukui.powermanagement.interface")
public:
    explicit SysdbusRegister();

    ~SysdbusRegister();

    /**
     * @brief executeLinuxCmd
     * @return 返回执行命令后返回内容
     * 调用linux终端
     */
    QString executeLinuxCmd(QString strCmd);

private:
    /**
     * @brief controlLogin1Connection
     * 创建与login1的dbus连接
     */
    void controlLogin1Connection(QString);

    QString canControl(const QString);

    /**
     * @brief setSuspendThenHibernate
     * 设置S3转S4时间（2小时）
     */
    void setSuspendThenHibernate();

    //目前用来区分CPU
    /**
     * @brief getCpuInfo
     * 获取CPU信息
     * @return
     */
    int getCpuInfo();

    /**
     * @brief getBacklightFile
     * 查看backlight中文件
     * @return
     */
    QString getBacklightFile(int);

    /**
     * @brief getBacklightFileNum
     * 获取backlight中文件个数
     * @return
     */
    int getBacklightFileNum();

    /**
     * @brief getCpuFreqMode
     * 获取CPU支持模式
     */
    void getCpuFreqMode();

    /**
     * @brief setCpuFreqMode
     * 设置CPU模式
     */
    void setCpuFreqMode(const QString mode);

    /**
     * @brief GetMaxBrightness
     * 获取屏幕亮度的最大值
     */

    qulonglong GetMaxBrightness();

    /**
     * @brief GetMaxBrightness
     * 获取屏幕当前亮度
     */
    int GetBrightness();
    qulonglong convertbrightness(const int brightness);

    int convertbrightness(const qulonglong sysBrightness);

    bool m_CanSetBrightness = true;

    QProcess m_process;

    QByteArray m_cpuType;

    QByteArray m_cpuMode;

    QSettings *m_susThenHibSet;

    QPointer<PowerConfig> m_conf;

    qulonglong m_maxBrightness;

    int m_modelName;

    int m_policyCpu;
    int m_policyGpu;
    int m_policy;

    int version;
    enum policy
    {
        Performance = 0,
        Balance,
        EnergySaving,

    };

    enum cpuModelName
    {
        OTHER = 0,
        Intel,
        ZHAOXIN,
        Phytium,
        Loongson3A5000,
        HUAWEI,
    };

signals:
//    Q_SCRIPTABLE void nameChanged(QString);

public slots:

    Q_SCRIPTABLE void ExitService();

    /**
     * @brief RegulateBrightness
     * 调整屏幕亮度
     */
    Q_SCRIPTABLE QString RegulateBrightness(int);

    /**
     * @brief TurnOffDisplay
     * 关闭显示器
     */
    Q_SCRIPTABLE void TurnOffDisplay();

    /**
     * @brief CpuFreqencyModulation
     * cpu调频策略修改
     */
    Q_SCRIPTABLE void CpuFreqencyModulation(const int);

    /**
     * @brief GpuFreqencyModulation
     * gpu调频策略修改
     */
    Q_SCRIPTABLE void GpuFreqencyModulation(const int);

    /**
     * @brief Hibernate
     * 重写休眠接口，以适应不同项目的休眠需求
     */
    Q_SCRIPTABLE void Hibernate(const bool);

    /**
     * @brief Suspend
     * 重写睡眠接口，以适应不同项目的休眠需求
     */
    Q_SCRIPTABLE void Suspend(const bool);
    /**
     * @brief PowerOff
     * 关机接口
     */
    Q_SCRIPTABLE void PowerOff(const bool);
    /**
     * @brief Reboot
     * 重启接口
     */
    Q_SCRIPTABLE void Reboot(const bool);

    /**
     * @brief SetPowerConfig
     * 设置电源配置项
     */
    Q_SCRIPTABLE void SetPowerConfig(const QString, const QDBusVariant);

    /**
     * @brief SuspendThenHibernate
     * 睡眠转休眠（时间默认设置为2小时）
     */
    Q_SCRIPTABLE void SuspendThenHibernate(const bool);

    /**
     * @brief TurnOffDisplay
     * 声音设置
     */
    Q_SCRIPTABLE void SetAudioMode(const int);

    /**
     * @brief TurnOffDisplay
     * pcie相关设置
     */
    Q_SCRIPTABLE QString SetPcieAspmMode(const int);

    /**
     * @brief CanSetBrightness
     * @return
     * 电源管理是否调整调整亮度
     */
    Q_SCRIPTABLE bool CanSetBrightness();
};

#endif // SYSDBUSREGISTER_H
