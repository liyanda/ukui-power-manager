/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sysdbusregister.h"

#include <QDebug>
#include <QSharedPointer>
#include <QRegExp>
#include <stdlib.h>
#include <QX11Info>

SysdbusRegister::SysdbusRegister() : m_conf(new PowerConfig)
{
    if (1 == getBacklightFileNum()) {
        m_modelName = OTHER;
    } else if (1 > getBacklightFileNum()) {
        m_CanSetBrightness = false;
    } else {
        m_modelName = getCpuInfo();
    }
    m_susThenHibSet = new QSettings(QString("/etc/systemd/sleep.conf"), QSettings::IniFormat, this);
    m_susThenHibSet->setIniCodec("UTF-8");
    getCpuFreqMode();
    if (m_CanSetBrightness) {
        m_maxBrightness = GetMaxBrightness();
        m_conf->initBrightness(GetBrightness());
    } else {
        m_maxBrightness = 1;
    }
}

SysdbusRegister::~SysdbusRegister() {}

void SysdbusRegister::ExitService()
{
    qApp->exit(0);
}

bool SysdbusRegister::CanSetBrightness()
{
    return m_CanSetBrightness;
}

QString SysdbusRegister::executeLinuxCmd(QString strCmd)
{
    //调用linux终端命令
    m_process.start("bash", QStringList() << "-c" << strCmd);
    m_process.waitForFinished();
    QString strResult = m_process.readAllStandardOutput() + m_process.readAllStandardError();
    return strResult;
}

QString SysdbusRegister::canControl(const QString control)
{
    QDBusInterface iface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        QDBusConnection::systemBus());
    QDBusReply<QString> reply = iface.call(control);
    if (reply.isValid()) {
        if ("yes" == reply.value()) {
            return reply;
        } else {
            QDBusMessage message = QDBusMessage::createSignal("/", "org.ukui.powermanagement.interface", "CanControl");
            message << reply.value();
            QDBusConnection::systemBus().send(message);
            return reply;
        }
    } else {
        return "error";
    }
}

void SysdbusRegister::controlLogin1Connection(QString type)
{
    bool ctrl = 1;
    QDBusInterface iface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        QDBusConnection::systemBus());
    iface.call(type, ctrl);
}

qulonglong SysdbusRegister::GetMaxBrightness()
{
    qulonglong maxValue;
    switch (m_modelName) {
        case ZHAOXIN:
            maxValue = executeLinuxCmd("cat /sys/class/backlight/acpi_video0/max_brightness").toInt();
            break;
        case Phytium:
            maxValue = executeLinuxCmd("cat /sys/class/backlight/ec_bl/max_brightness").toInt();
            break;
        case Loongson3A5000:
            maxValue = executeLinuxCmd(
                           QString("cat /sys/class/backlight/%1/max_brightness").arg(getBacklightFile(Loongson3A5000)))
                           .toInt();
            break;
        case OTHER:
            maxValue = executeLinuxCmd("cat /sys/class/backlight/*/max_brightness").toInt();
            break;
        default:
            break;
    }
    return maxValue;
}

int SysdbusRegister::GetBrightness()
{
    qulonglong value;
    switch (m_modelName) {
        case ZHAOXIN:
            value = executeLinuxCmd("cat /sys/class/backlight/acpi_video0/actual_brightness").toInt();
            break;
        case Phytium:
            value = executeLinuxCmd("cat /sys/class/backlight/ec_bl/actual_brightness").toInt();
            break;
        case Loongson3A5000:
            value = executeLinuxCmd(
                        QString("cat /sys/class/backlight/%1/actual_brightness").arg(getBacklightFile(Loongson3A5000)))
                        .toInt();
            break;
        case OTHER:
            value = executeLinuxCmd("cat /sys/class/backlight/*/actual_brightness").toInt();
            break;
        default:
            break;
    }   
    return convertbrightness(value);
}

QString SysdbusRegister::RegulateBrightness(int brightness)
{
    QString msg;
    qulonglong sysBrightness = convertbrightness(brightness);
    switch (m_modelName) {
        case ZHAOXIN:
            msg = executeLinuxCmd(QString("echo %1 | tee /sys/class/backlight/acpi_video0/brightness").arg(brightness));
            break;
        case Phytium:
            msg = executeLinuxCmd(QString("echo %1 | tee /sys/class/backlight/ec_bl/brightness").arg(sysBrightness));
            break;
        case Loongson3A5000:
            msg = executeLinuxCmd(QString("echo %1 | tee /sys/class/backlight/%2/brightness")
                                      .arg(sysBrightness)
                                      .arg(getBacklightFile(Loongson3A5000)));
            break;
        case OTHER:
            msg = executeLinuxCmd(QString("echo %1 | tee /sys/class/backlight/*/brightness").arg(sysBrightness));
            break;
        default:
            break;
    }
    msg = QString("%1 brightness").arg(sysBrightness);
    return msg;
}

int SysdbusRegister::convertbrightness(const qulonglong sysBrightness)
{
    float brightness = sysBrightness * 100 / m_maxBrightness;
    return brightness;
}

qulonglong SysdbusRegister::convertbrightness(const int brightness)
{
    qulonglong sysbrightness;
    if (1 > brightness) {
        sysbrightness = m_maxBrightness * 0.01;
    } else {
        if (100 < brightness) {
            sysbrightness = m_maxBrightness;
        } else {
            sysbrightness = brightness * m_maxBrightness * 0.01;
        }
    }
    return sysbrightness;
}

QString SysdbusRegister::getBacklightFile(int type)
{
    if (Loongson3A5000 == type) {
        QFile loongsonGpuFile("/sys/class/backlight/loongson-gpu");
        if (loongsonGpuFile.exists()) {
            return QString("loongson-gpu");
        } else {
            return QString("loongson_laptop");
        }
    } else {
        return QString("*");
    }
}

int SysdbusRegister::getBacklightFileNum()
{
    QDir dir("/sys/class/backlight");
    QStringList filter;
    QStringList filterAll;
    filter << ".*";
    dir.setNameFilters(filter);
    QList<QFileInfo> fileInfo = dir.entryInfoList(filter);
    int num = fileInfo.count();
    fileInfo = dir.entryInfoList(filterAll);
    int numAll = fileInfo.count();
    return numAll - num;
}

void SysdbusRegister::TurnOffDisplay()
{
    //关闭显示器由后台处理
}

void SysdbusRegister::CpuFreqencyModulation(const int strategy)
{
    if (m_policy == strategy) {
        return;
    }
    m_policy = strategy;
    switch (m_policy) {
    case Performance:
        setCpuFreqMode("performance");
        break;
    case Balance:
        if (-1 == m_cpuMode.indexOf("schedutil")) {
            if (-1 == m_cpuMode.indexOf("ondemand")) {
                setCpuFreqMode("powersave");
            } else {
                setCpuFreqMode("ondemand");
            }
        } else {
            setCpuFreqMode("schedutil");
        }
        break;
    case EnergySaving:
        setCpuFreqMode("powersave");
        break;
    default:
        break;
    }
}

void SysdbusRegister::getCpuFreqMode()
{
    QFile file("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors");
    if (!file.open(QIODevice::ReadOnly)) {
        qCritical() << "get svaling available governors error:" << file.errorString();
    }
    m_cpuMode = file.readAll();
    file.close();
}

void SysdbusRegister::setCpuFreqMode(const QString mode)
{
    int cpuQuantity = executeLinuxCmd("grep -c 'processor' /proc/cpuinfo").toInt();
    for (int var = 0; var < cpuQuantity; ++var) {
        executeLinuxCmd(
            QString("echo %1 | tee /sys/devices/system/cpu/cpu%2/cpufreq/scaling_governor").arg(mode).arg(var));
    }
}

void SysdbusRegister::GpuFreqencyModulation(int strategy)
{
    QFile radeonFile("/sys/class/drm/card0/device/power_dpm_state");
    QFile amdgpuFile("/sys/class/drm/card0/device/power_dpm_force_performance_level");
    if (radeonFile.exists()) {
        switch (strategy) {
            case Performance:
                executeLinuxCmd(QString("echo performance | tee /sys/class/drm/card0/device/power_dpm_state"));
                break;
            case Balance:
                executeLinuxCmd(QString("echo balanced | tee /sys/class/drm/card0/device/power_dpm_state"));
                break;
            case EnergySaving:
                executeLinuxCmd(QString("echo battery | tee /sys/class/drm/card0/device/power_dpm_state"));
                break;
            default:
                break;
        }
    }
    if (amdgpuFile.exists()) {
        switch (strategy) {
            case Performance:
                executeLinuxCmd(
                    QString("echo high | tee /sys/class/drm/card0/device/power_dpm_force_performance_level"));
                break;
            case Balance:
                executeLinuxCmd(
                    QString("echo auto | tee /sys/class/drm/card0/device/power_dpm_force_performance_level"));
                break;
            case EnergySaving:
                executeLinuxCmd(
                    QString("echo low | tee /sys/class/drm/card0/device/power_dpm_force_performance_level"));
                break;
            default:
                break;
        }
    }
}

void SysdbusRegister::SetAudioMode(int strategy)
{
    if (Performance == strategy) {
        executeLinuxCmd(QString("echo 1 | tee /sys/module/snd_hda_intel/parameters/power_save"));
    } else {
        executeLinuxCmd(QString("echo 0 | tee /sys/module/snd_hda_intel/parameters/power_save"));
    }
}

QString SysdbusRegister::SetPcieAspmMode(int strategy)
{
    QString str = "null";
    switch (getCpuInfo()) {
        case Phytium:
            str = "phytium";
            break;
        case Loongson3A5000:
        case ZHAOXIN:
        case OTHER:
            if (Performance == strategy) {
                str = executeLinuxCmd(QString("echo performance | tee /sys/module/pcie_aspm/parameters/policy"));
            } else {
                str = executeLinuxCmd(QString("echo powersave | tee /sys/module/pcie_aspm/parameters/policy"));
            }
            break;
        default:
            str = "unknow";
            break;
    }
    return str;
}

void SysdbusRegister::SetPowerConfig(const QString key, const QDBusVariant value)
{
    m_conf->updateConfData(key, value.variant());
}

void SysdbusRegister::Hibernate(const bool state)
{
    if ("yes" == canControl("CanHibernate") && state) {
        controlLogin1Connection("Hibernate");
    }
}

void SysdbusRegister::Suspend(const bool state)
{
    if ("yes" == canControl("CanSuspend") && state) {
        controlLogin1Connection("Suspend");
    }
}

void SysdbusRegister::PowerOff(const bool state)
{
    if ("yes" == canControl("CanPowerOff") && state) {
        controlLogin1Connection("PowerOff");
    }
}

void SysdbusRegister::Reboot(const bool state)
{
    if ("yes" == canControl("CanReboot") && state) {
        controlLogin1Connection("Reboot");
    }
}

void SysdbusRegister::SuspendThenHibernate(const bool state)
{
    if ("yes" == canControl("CanSuspendThenHibernate") && state) {
        m_susThenHibSet->beginGroup("Sleep");
        m_susThenHibSet->setValue("HibernateDelaySec", 7200);
        m_susThenHibSet->endGroup();
        m_susThenHibSet->sync();
        controlLogin1Connection("SuspendThenHibernate");
    }
}

int SysdbusRegister::getCpuInfo()
{
    QFile file("/proc/cpuinfo");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
    }
    m_cpuType = file.readAll();
    QString type = m_cpuType;
    file.close();
    if (-1 != type.indexOf("ZHAOXIN", 0, Qt::CaseInsensitive)) {
        return ZHAOXIN;
    } else if (-1 != type.indexOf("D2000", 0, Qt::CaseInsensitive)) {
        return Phytium;
    } else if (-1 != type.indexOf("3A5000", 0, Qt::CaseInsensitive)) {
        return Loongson3A5000;
    } else if (-1 != type.indexOf("Phytium", 0, Qt::CaseInsensitive)) {
        return Phytium;
    } else {
        return OTHER;
    }
}
