#ifndef POWERCONFIG_H
#define POWERCONFIG_H

#include "../common/common.h"
#include <QSettings>
#include <QFile>

#define POWER_MANAGER_CONF_PATH "/etc/power/power-manager.conf"

class PowerConfig : public QObject
{
    Q_OBJECT
public:
    explicit PowerConfig(QObject *parent = Q_NULLPTR);
    ~PowerConfig();
    void updateConfData(const QString &key, const QVariant &value);
    void initBrightness(const int brightness);
private:
    void initPowerConfig();
    void vendorSpecialTreatment();
    QString getProduct();
    int getManufacturer();
    int vendorTypeMatch(const QString &type);
    bool isNeedConfig();

    QPointer<QSettings> m_settings;
    enum Vendor
    {
        UnKnown = 0,
        Greatwall,
    };
};

#endif // POWERCONFIG_H
