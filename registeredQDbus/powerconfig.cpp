#include "powerconfig.h"

PowerConfig::PowerConfig(QObject *parent) : QObject(parent)
        , m_settings(new QSettings(POWER_MANAGER_CONF_PATH, QSettings::NativeFormat))
{
    if (isNeedConfig()) {
        initPowerConfig();
        m_settings->beginGroup(CONFIG);
        m_settings->setValue(INIT_POWER_MANAGER, false);
        m_settings->sync();
        m_settings->endGroup();
    }
}

PowerConfig::~PowerConfig()
{

}

bool PowerConfig::isNeedConfig()
{
    bool ret(false);
    m_settings->beginGroup(CONFIG);;
    ret = m_settings->value(INIT_POWER_MANAGER).toBool();
    m_settings->endGroup();
    if (ret) {
        qDebug() << "power manager need init config";
    } else {
        qDebug() << "power manager need't init config";
    }
    return ret;
}

void PowerConfig::initBrightness(const int brightness)
{
    int ret;
    m_settings->sync();
    m_settings->beginGroup(CONFIG);;
    ret = m_settings->value(BRIGHTNESS).toInt();
    if (0 > ret) {
        m_settings->setValue(BRIGHTNESS, brightness);
    }
    m_settings->endGroup();
    m_settings->sync();
}

void PowerConfig::initPowerConfig()
{
    QStringList str;
    QString proName;
    QHash<QString, QVariant> cache;
    proName = getProduct();
    m_settings->beginGroup("product");
    str = m_settings->childKeys();
    m_settings->endGroup();
    if (-1 == str.indexOf(proName)) {
        qDebug() << "vendor special treatment";
        vendorSpecialTreatment();
    } else {
        qDebug() << "produce special treatement";
        m_settings->beginGroup(proName);
        str = m_settings->childKeys();
        for (int var = 0; var < str.size(); ++var) {
            cache.insert(str.at(var), m_settings->value(str.at(var)));
        }
        m_settings->endGroup();
        m_settings->beginGroup(CONFIG);
        QHash<QString, QVariant>::iterator i;
        for (i = cache.begin(); i != cache.end(); ++i) {
            m_settings->setValue(i.key(), i.value());
        }
        m_settings->endGroup();
        m_settings->sync();
    }
    qDebug() << "config success";
}

QString PowerConfig::getProduct()
{
    QString name;
    QFile file("/sys/class/dmi/id/product_name");
    if (!file.open(QIODevice::ReadOnly)) {
        qCritical() << "open product name file error msg: " << file.errorString();
        return name;
    }
    QByteArray data = file.readAll();
    file.close();
    name.prepend(data);
    name = name.simplified();
    qDebug() << "product name is :" << name;
    return name;
}

int PowerConfig::getManufacturer()
{
    QString vendor;
    QFile file("/sys/class/dmi/id/board_vendor");
    if (!file.open(QIODevice::ReadOnly)) {
        qCritical() << "open board vendor file error msg: " << file.errorString();
        return UnKnown;
    }
    QByteArray data = file.readAll();
    file.close();
    if (data.isEmpty()) {
        return UnKnown;
    }
    vendor.prepend(data);
    vendor = vendor.simplified();
    return vendorTypeMatch(vendor);
}

int PowerConfig::vendorTypeMatch(const QString &type)
{
    if (-1 != type.indexOf("Greatwall",0,Qt::CaseInsensitive)) {
        qDebug() << "Greatwall";
        return Greatwall;
    } else {
        qDebug() << "unknow";
        return UnKnown;
    }
}

void PowerConfig::vendorSpecialTreatment()
{
    switch (getManufacturer()) {
        case Greatwall:
            updateConfData("powerPolicyBattery", 1);
            break;
        default:
            break;
    }
}

void PowerConfig::updateConfData(const QString &key, const QVariant &value)
{
    m_settings->sync();
    m_settings->beginGroup(CONFIG);
    if (m_settings->value(key) != value) {
        qCritical() << "config data will changed," << key << value;
        m_settings->setValue(key, value);
        m_settings->sync();
    } else {
        qCritical() << key << "No update required" << value << "config data is :" << m_settings->value(key);
    }
    m_settings->endGroup();
}
