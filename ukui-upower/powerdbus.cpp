/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "powerdbus.h"

PowerDbus::PowerDbus()
{
    m_manager = new PowerManager;
    connect(m_manager, &PowerManager::configChanged, this, &PowerDbus::dealConfigChanged);
}

PowerDbus::~PowerDbus()
{
    delete m_manager;
}

void PowerDbus::SetActionCriticalBat(const QString value)
{
    showServiceMsg();
    m_manager->setActionCriticalBat(value);
}

void PowerDbus::SetBatterySaver(const int value)
{
    showServiceMsg();
    m_manager->setBatterySaver(value);
}

void PowerDbus::SetBatterySaverAuto(const bool value)
{
    showServiceMsg();
    m_manager->setBatterySaverAuto(value);
}

void PowerDbus::SetBatterySaverBrightness(const bool value)
{
    showServiceMsg();
    m_manager->setBatterySaverBrightness(value);
}

void PowerDbus::SetBatterySaverPercent(const int value)
{
    showServiceMsg();
    m_manager->setBatterySaverPercent(value);
}

void PowerDbus::SetBrightness(const int value)
{
    showServiceMsg();
    m_manager->setBrightness(value);
}

QString PowerDbus::SetButtonLidAc(const QString value)
{
    showServiceMsg();
    return m_manager->setButtonLidAc(value);
}

QString PowerDbus::SetButtonLidBat(const QString value)
{
    showServiceMsg();
    return m_manager->setButtonLidBat(value);
}

void PowerDbus::SetCriticalBattery(const int value)
{
    showServiceMsg();
    m_manager->setCriticalBattery(value);
}

void PowerDbus::SetLowBattery(const int value)
{
    showServiceMsg();
    m_manager->setLowBattery(value);
}

void PowerDbus::SetPowerPolicyAc(const int value)
{
    showServiceMsg();
    m_manager->setPowerPolicyAc(value);
}

void PowerDbus::SetPowerPolicyBat(const int value)
{
    showServiceMsg();
    m_manager->setPowerPolicyBat(value);
}

void PowerDbus::SetSleepComputerAc(const int value)
{
    showServiceMsg();
    m_manager->setSleepComputerAc(value);
}

void PowerDbus::SetSleepComputerBat(const int value)
{
    showServiceMsg();
    m_manager->setSleepComputerBat(value);
}

void PowerDbus::SetSleepDisplayAc(const int value)
{
    showServiceMsg();
    m_manager->setSleepDisplayAc(value);
}

void PowerDbus::SetSleepDisplayBat(const int value)
{
    showServiceMsg();
    m_manager->setSleepDisplayBat(value);
}

QString PowerDbus::GetActionCriticalBat()
{
    showServiceMsg();
    return m_manager->getActionCriticalBat();
}

int PowerDbus::GetBatterySaver()
{
    showServiceMsg();
    return m_manager->getBatterySaver();
}

bool PowerDbus::GetBatterySaverAuto()
{
    showServiceMsg();
    return m_manager->getBatterySaverAuto();
}

bool PowerDbus::GetBatterySaverBrightness()
{
    showServiceMsg();
    return m_manager->getBatterySaverBrightness();
}

int PowerDbus::GetBatterySaverPercent()
{
    showServiceMsg();
    return m_manager->getBatterySaverPercent();
}

int PowerDbus::GetBrightness()
{
    showServiceMsg();
    return m_manager->getBrightness();
}

QString PowerDbus::GetButtonLidAc()
{
    showServiceMsg();
    return m_manager->getButtonLidAc();
}

QString PowerDbus::GetButtonLidBat()
{
    showServiceMsg();
    return m_manager->getButtonLidBat();
}

int PowerDbus::GetCriticalBattery()
{
    showServiceMsg();
    return m_manager->getCriticalBattery();
}

int PowerDbus::GetLowBattery()
{
    showServiceMsg();
    return m_manager->getLowBattery();
}

int PowerDbus::GetPowerPolicyAc()
{
    showServiceMsg();
    return m_manager->getPowerPolicyAc();
}

int PowerDbus::GetPowerPolicyBat()
{
    showServiceMsg();
    return m_manager->getPowerPolicyBat();
}

int PowerDbus::GetSleepComputerAc()
{
    showServiceMsg();
    return m_manager->getSleepComputerAc();
}

int PowerDbus::GetSleepComputerBat()
{
    showServiceMsg();
    return m_manager->getSleepComputerBat();
}

int PowerDbus::GetSleepDisplayAc()
{
    showServiceMsg();
    return m_manager->getSleepDisplayAc();
}

int PowerDbus::GetSleepDisplayBat()
{
    showServiceMsg();
    return m_manager->getSleepDisplayBat();
}

void PowerDbus::dealConfigChanged(QStringList list)
{
    emit PowerConfigChanged(list);
}
