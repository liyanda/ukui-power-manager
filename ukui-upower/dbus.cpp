/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "dbus.h"

DBus::DBus(QObject *parent) : QObject(parent), QDBusContext(){}

DBus::~DBus() {}

void DBus::showServiceMsg()
{
    QDBusConnection con = connection();
    QDBusMessage msg = message();
    int str = con.interface()->servicePid(msg.service()).value();
    qDebug() << "Sender Name" << con.interface()->serviceOwner(msg.service()).value();
    qDebug() << "Sender Uid" << con.interface()->serviceUid(msg.service()).value();
    qDebug() << "Sender Pid " << str;
    qDebug() << executeLinuxCmd(QString::number(str));
}

QString DBus::executeLinuxCmd(QString strCmd)
{
    QProcess process;
    process.start("bash", QStringList() << "-c" << QString("ps -p %1 -o comm=").arg(strCmd));
    process.waitForFinished();
    QString strResult = process.readAllStandardOutput() + process.readAllStandardError();
    return strResult;
}
