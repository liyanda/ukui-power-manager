/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef POWERDBUS_H
#define POWERDBUS_H

#include <dbus.h>
#include <powermanager/powermanager.h>
class PowerDbus : public DBus
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.ukui.powerManager")
public:
    PowerDbus();
    ~PowerDbus();

public slots:
    //电源管理参数设置接口
    void SetPowerPolicyAc(const int);
    void SetPowerPolicyBat(const int);
    void SetSleepDisplayAc(const int);
    void SetSleepDisplayBat(const int);
    void SetSleepComputerAc(const int);
    void SetSleepComputerBat(const int);
    void SetLowBattery(const int);
    void SetCriticalBattery(const int);
    void SetBatterySaver(const int);
    void SetBatterySaverPercent(const int);
    void SetBrightness(const int);
    void SetBatterySaverAuto(const bool);
    void SetBatterySaverBrightness(const bool);
    void SetActionCriticalBat(const QString);
    QString SetButtonLidAc(const QString);
    QString SetButtonLidBat(const QString);

    //电源管理数据参数获取接口
    int GetPowerPolicyAc();
    int GetPowerPolicyBat();
    int GetSleepDisplayAc();
    int GetSleepDisplayBat();
    int GetSleepComputerAc();
    int GetSleepComputerBat();
    int GetLowBattery();
    int GetCriticalBattery();
    int GetBatterySaver();
    int GetBatterySaverPercent();
    int GetBrightness();
    bool GetBatterySaverAuto();
    bool GetBatterySaverBrightness();
    QString GetActionCriticalBat();
    QString GetButtonLidAc();
    QString GetButtonLidBat();
signals:
    void PowerConfigChanged(QStringList type);

private:
    void dealConfigChanged(QStringList);
    PowerManager *m_manager;
};

#endif // POWERDBUS_H
