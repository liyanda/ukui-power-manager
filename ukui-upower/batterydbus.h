/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef BATTERYDBUS_H
#define BATTERYDBUS_H

#include <dbus.h>
#include <batteryinfo/batteryinfo.h>
#include "../common/common.h"
class BatteryDBus : public DBus
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.ukui.upower.battery")
public:
    BatteryDBus();

    ~BatteryDBus();

public slots:

    /**
     * @brief IconName
     * @return
     * 图标名称
     */
    QString IconName();

    /**
     * @brief Percentage
     * @return
     * 电量百分比
     */
    double Percentage();

    /**
     * @brief TimeToFull
     * @return
     * 充电过程中，还有多久充满电
     */
    int TimeToFull();

    /**
     * @brief TimeToEmpty
     * @return
     * 放电过程 还有多久耗尽
     */
    int TimeToEmpty();

    /**
     * @brief IsPresent
     * 电池是否存在
     * @return
     */
    bool IsPresent();

    /**
     * @brief BatteryState
     * 电池状态
     * @return
     */
    int BatteryState();

    /**
     * @brief LowBattery
     * 低电量
     * @return
     */
    int LowBattery();

    /**
     * @brief WarningLevel
     * @return
     */
    int WarningLevel();

    /**
     * @brief BatteryAutoState
     * @return
     */
    bool BatteryAutoState();
signals:

    /**
     * @brief lowBatteryState
     * @return
     * 低电量状态信号
     */
    void LowBatteryState(int);

    void BatteryState(int);

    void BatteryIcon(QString);

    void BatterySaverAuto(bool);

private:
    BatteryInfo *batteryinfo;

    void dealLowBattery(int);

    void dealBatteryState(int);

    void dealBatteryIcon(QString);

    void dealBatterySaver(bool);
};

#endif // BATTERYDBUS_H
