/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef POWERMANAGER_H
#define POWERMANAGER_H

#include <QObject>
#include <QtDBus>
#include <../common/common.h>

struct SysData
{
    int sysPowerPolicyAc;
    int sysPowerPolicyBat;
    int sysSleepDisplayAc;
    int sysSleepDisplayBat;
    int sysSleepComputerAc;
    int sysSleepComputerBat;
    int sysLowBattery;
    int sysCriticalBattery;
    int sysBatterySaver;
    int sysBatterySaverPercent;
    int sysBrightness;
    bool sysBatterySaverAuto;
    bool sysBatterySaverBrightness;
    QString sysActionCriticalBat;
    QString sysButtonLidAc;
    QString sysButtonLidBat;
};

class PowerManager : public QObject
{
    Q_OBJECT
public:
    explicit PowerManager(QObject *parent = nullptr);
    ~PowerManager();

    void setPowerPolicyAc(const int &value);
    void setPowerPolicyBat(const int &value);
    void setSleepDisplayAc(const int &value);
    void setSleepDisplayBat(const int &value);
    void setSleepComputerAc(const int &value);
    void setSleepComputerBat(const int &value);
    void setLowBattery(const int &value);
    void setCriticalBattery(const int &value);
    void setBatterySaver(const int &value);
    void setBatterySaverPercent(const int &value);
    void setBrightness(const int &value);
    void setBatterySaverAuto(const bool &value);
    void setBatterySaverBrightness(const bool &value);
    void setActionCriticalBat(const QString &value);
    QString setButtonLidAc(const QString &value);
    QString setButtonLidBat(const QString &value);

    int getPowerPolicyAc();
    int getPowerPolicyBat();
    int getSleepDisplayAc();
    int getSleepDisplayBat();
    int getSleepComputerAc();
    int getSleepComputerBat();
    int getLowBattery();
    int getCriticalBattery();
    int getBatterySaver();
    int getBatterySaverPercent();
    int getBrightness();
    bool getBatterySaverAuto();
    bool getBatterySaverBrightness();
    QString getActionCriticalBat();
    QString getButtonLidAc();
    QString getButtonLidBat();
//    SysData getSysData();

signals:
    void configChanged(QStringList type);

private:
    void getConfData();
    void setConfigValue(const QString key, const QVariant value);
    QVariant getPowerValue(const QString &key);
    QStringList diffSysData();
    bool compareSetValue(const QString &value);
    template<typename data>
    QString dataJudgment(const QString &type, data &D1, data &D2);

    QPointer<QSettings> m_settings;
    QPointer<QFileSystemWatcher> m_fileWatcher;
    QPointer<QDBusInterface> m_iface;
    SysData m_sysData;

};

#endif // POWERMANAGER_H
