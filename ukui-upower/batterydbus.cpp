/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "batterydbus.h"

BatteryDBus::BatteryDBus()
{
    batteryinfo = new BatteryInfo;
    connect(batteryinfo, &BatteryInfo::LowBatteryChanged, this, &BatteryDBus::dealLowBattery);
    connect(batteryinfo, &BatteryInfo::BatteryStateChanged, this, &BatteryDBus::dealBatteryState);
    connect(batteryinfo, &BatteryInfo::BatteryIconChanged, this, &BatteryDBus::dealBatteryIcon);
    connect(batteryinfo, &BatteryInfo::BatterySaverAutoChanged, this, &BatteryDBus::dealBatterySaver);
}

BatteryDBus::~BatteryDBus()
{
    delete batteryinfo;
}

QString BatteryDBus::IconName()
{
    showServiceMsg();
    return batteryinfo->IconName();
}

double BatteryDBus::Percentage()
{
    showServiceMsg();
    return batteryinfo->Percentage();
}

int BatteryDBus::TimeToFull()
{
    showServiceMsg();
    return batteryinfo->TimeToFull();
}

int BatteryDBus::TimeToEmpty()
{
    showServiceMsg();
    return batteryinfo->TimeToEmpty();
}

bool BatteryDBus::IsPresent()
{
    showServiceMsg();
    return batteryinfo->IsPresent();
}

int BatteryDBus::BatteryState()
{
    showServiceMsg();
    return batteryinfo->getBatteryState();
}

int BatteryDBus::LowBattery()
{
    showServiceMsg();
    return batteryinfo->LowBatteryState();
}

int BatteryDBus::WarningLevel()
{
    showServiceMsg();
    return batteryinfo->getWarningLevel();
}

bool BatteryDBus::BatteryAutoState()
{
    showServiceMsg();
    return batteryinfo->getBatterySaverState();
}

void BatteryDBus::dealBatteryState(int state)
{
    emit BatteryState(state);
}

void BatteryDBus::dealLowBattery(int state)
{
    emit LowBatteryState(state);
}

void BatteryDBus::dealBatteryIcon(QString msg)
{
    emit BatteryIcon(msg);
}

void BatteryDBus::dealBatterySaver(bool state)
{
    emit BatterySaverAuto(state);
}
