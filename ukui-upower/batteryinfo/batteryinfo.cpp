/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include "batteryinfo.h"

BatteryInfo::BatteryInfo(QObject *parent)
    : QObject(parent)
    , m_upower(new UPowerInfo)
    , m_settings(new QSettings(POWER_MANAGER_CONF_PATH, QSettings::NativeFormat))
    , m_fileWatcher(new QFileSystemWatcher)
{
    if (m_settings->status() != QSettings::NoError) {
        qCritical() << "qsettings init error!";
    }
    QString batteryPath = m_upower->getBatteryPath();
    m_iface = QSharedPointer<QDBusInterface>(
        new QDBusInterface(UPOWER_SERVICE, batteryPath, FREEDESKTOP_UPOWER, QDBusConnection::systemBus()));
    initBatteryInfo(m_bat1);
    showBatteryInfo();
    initConnect(batteryPath);
}

BatteryInfo::~BatteryInfo() {}

void BatteryInfo::initBatteryInfo(BatInfo &data)
{
    readSettings();
    initLowBatteryState();
    QDBusMessage msg = m_iface->call("GetAll", "org.freedesktop.UPower.Device");
    qDebug() << "get all bat msg type :" << msg.type();
    if (msg.type() == QDBusMessage::ReplyMessage) {
        const QDBusArgument &dbusArg = msg.arguments().at(0).value<QDBusArgument>();
        QMap<QString, QVariant> map;
        dbusArg >> map;
        data.isPresent = (map.value(QString("IsPresent")).toBool());
        data.percentage = map.value(QString("Percentage")).toDouble();
        data.percentage = ((float)((int)((data.percentage + 0.05) * 10))) / 10;
        data.state = map.value(QString("State")).toInt();
        data.timeToEmpty = map.value(QString("TimeToEmpty")).toLongLong();
        data.timeToFull = map.value(QString("TimeToFull")).toLongLong();
        data.warninglevel = map.value(QString("WarningLevel")).toInt();
    } else {
        qDebug() << "init battery data faile";
    }
}

void BatteryInfo::initConnect(const QString &batPath)
{
    m_fileWatcher->addPath(POWER_MANAGER_CONF_PATH);
    connect(m_fileWatcher, &QFileSystemWatcher::fileChanged, this, [=](QString filePath) {
        qDebug() << "file changed" << m_fileWatcher->files();
        m_fileWatcher->addPath(filePath);
        m_settings->sync();
        readSettings();
        getBatteryInfo();
        dealLowBatteryMessage(m_batteryState);
        dealBatterySaverAutoMessage(m_batteryState, m_percentage);
    });
    QDBusConnection::systemBus().connect(
        UPOWER_SERVICE, batPath, FREEDESKTOP_UPOWER, "PropertiesChanged", this, SLOT(dealMessage(QDBusMessage)));
    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForSleep",
        this,
        SLOT(dealSleepChanged(bool)));
}

void BatteryInfo::initLowBatteryState()
{
    m_lowBatteryState = LowBatteryState();
    if (Critical == getWarningLevel()) {
        m_veryLowBatteryState = true;
        m_lowBatteryState = true;
    } else {
        if (Low == LowBatteryState()) {
            m_lowBatteryState = true;
        } else {
            m_lowBatteryState = true;
        }
        m_veryLowBatteryState = false;
    }
}

void BatteryInfo::dealMessage(QDBusMessage msg)
{
    msgAnalysis(msg);
    getBatteryInfo();
    dealLowBatteryMessage(m_batteryState);
    dealBatteryIconMessage(m_batteryState, m_percentage);
    dealBatterySaverAutoMessage(m_batteryState, m_percentage);
}

void BatteryInfo::dealSleepChanged(bool state)
{
    if (!state && m_veryLowBatteryState) {
        emit LowBatteryChanged(Critical);
    }
}

void BatteryInfo::readSettings()
{
    m_settings->beginGroup("config");
    m_setPercentageLow = m_settings->value(LOW_BATTERY).toInt();
    m_percentageAction = m_settings->value(CRITICAL_BATTERY).toInt();
    m_batterySaverAuto = m_settings->value(BATTERY_SAVER_AUTO).toBool();
    m_batterySaverPercent = m_settings->value(BATTERY_SAVER_PERCENT).toInt();
    m_settings->endGroup();
    qDebug() << "low battery:" << m_setPercentageLow << "criticla battery:" << m_percentageAction;
}

void BatteryInfo::getBatteryInfo()
{
    QThread::msleep(500);
    m_batteryState = m_upower->OnBattery();
    m_percentage = Percentage();
}

void BatteryInfo::msgAnalysis(QDBusMessage batData)
{
    const QDBusArgument &dbusArgs = batData.arguments().at(1).value<QDBusArgument>();
    QMap<QString, QVariant> map;
    dbusArgs >> map;
    putBatteryInfo(map, m_bat1);
    showBatteryInfo();
}

void BatteryInfo::putBatteryInfo(QMap<QString, QVariant> &map, BatInfo &data)
{
    if (map.contains("TimeToFull")) {
        data.timeToFull = map.value(QString("TimeToFull")).toLongLong();
    }
    if (map.contains("TimeToEmpty")) {
        data.timeToEmpty = map.value(QString("TimeToEmpty")).toLongLong();
    }
    if (map.contains("WarningLevel")) {
        data.warninglevel = map.value(QString("WarningLevel")).toInt();
    }
    if (map.contains("State")) {
        data.state = map.value(QString("State")).toInt();
        emit BatteryStateChanged(data.state);
    }
    if (map.contains("Percentage")) {
        data.percentage = map.value(QString("Percentage")).toDouble();
        data.percentage = ((float)((int)((data.percentage + 0.05) * 10))) / 10;
    }
    if (map.contains("IsPresent")) {
        data.isPresent = (map.value(QString("IsPresent")).toBool());
    }
}

void BatteryInfo::showBatteryInfo()
{
    qDebug() << "TimeToFull" << m_bat1.timeToFull;
    qDebug() << "TimeToEmpty" << m_bat1.timeToEmpty;
    qDebug() << "WarningLevel" << m_bat1.warninglevel;
    qDebug() << "State" << m_bat1.state;
    qDebug() << "Percentage" << m_bat1.percentage;
    qDebug() << "IsPresent" << m_bat1.isPresent;
}

QString BatteryInfo::IconName()
{
    getBatteryInfo();
    double num = m_percentage;
    if (true == m_batteryState) {
        m_iconMessage = QString("battery-level-%1-symbolic").arg((int)num / 10 * 10);
    } else {
        m_iconMessage = QString("battery-level-%1-charging-symbolic").arg((int)num / 10 * 10);
    }
    return m_iconMessage;
}

int BatteryInfo::LowBatteryState()
{
    if (m_setPercentageLow > m_percentage) {
        return Low;
    } else {
        return None;
    }
}

double BatteryInfo::Percentage()
{
    return getPropertyValue("Percentage").toDouble();
}

int BatteryInfo::TimeToFull()
{
    return getPropertyValue("TimeToFull").toInt();
}

int BatteryInfo::TimeToEmpty()
{
    return getPropertyValue("TimeToEmpty").toInt();
}

bool BatteryInfo::IsPresent()
{
    return getPropertyValue("IsPresent").toBool();
}

int BatteryInfo::getBatteryState()
{
    return getPropertyValue("State").toInt();
}

int BatteryInfo::getWarningLevel()
{
    return getPropertyValue("WarningLevel").toInt();
}

bool BatteryInfo::getBatterySaverState()
{
    return m_batterySaverAutoState;
}

QVariant BatteryInfo::getPropertyValue(const QString &method)
{
    QDBusReply<QVariant> reply = m_iface->call("Get", UPOWER_DIVICES_SERVICE, method);
    if (reply.isValid()) {
        return reply.value();
    } else {
        qCritical() << QString("Get %1 failed").arg(method);
        return 0;
    }
}

void BatteryInfo::dealLowBatteryMessage(const bool &state)
{
    if (Critical == getWarningLevel() && state) {
        if (!m_veryLowBatteryState) {
            m_lowBatteryState = true;
            m_veryLowBatteryState = true;
            emit LowBatteryChanged(Critical);
        } else {
            return;
        }
    } else if (m_setPercentageLow > m_percentage && state) {
        if (!m_lowBatteryState) {
            m_lowBatteryState = true;
            m_veryLowBatteryState = false;
            emit LowBatteryChanged(Low);
        } else {
            return;
        }
    } else if (m_lowBatteryState || m_veryLowBatteryState) {
        m_lowBatteryState = false;
        m_veryLowBatteryState = false;
        emit LowBatteryChanged(None);
    }
}

void BatteryInfo::dealBatteryIconMessage(const bool &state, const double &precentage)
{
    double num = precentage;
    if (true == state) {
        m_iconMessage = QString("battery-level-%1-symbolic").arg((int)num / 10 * 10);
    } else {
        m_iconMessage = QString("battery-level-%1-charging-symbolic").arg((int)num / 10 * 10);
    }
    emit BatteryIconChanged(m_iconMessage);
}

void BatteryInfo::dealBatterySaverAutoMessage(const bool &state, const double &precentage)
{
    qDebug() << state << precentage << m_batterySaverAutoState << m_batterySaverPercent << m_batterySaverAuto;
    if (false == state) {
        if (m_batterySaverAutoState) {
            m_batterySaverAutoState = false;
            emit BatterySaverAutoChanged(m_batterySaverAutoState);
        }
        return;
    }
    if (precentage <= m_batterySaverPercent && m_batterySaverAuto) {
        if (!m_batterySaverAutoState) {
            m_batterySaverAutoState = true;
            emit BatterySaverAutoChanged(m_batterySaverAutoState);
        }
    } else {
        if (m_batterySaverAutoState) {
            m_batterySaverAutoState = false;
            emit BatterySaverAutoChanged(m_batterySaverAutoState);
        }
    }
}
