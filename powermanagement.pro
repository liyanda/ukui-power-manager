TEMPLATE = subdirs
  
CONFIG += ordered

SUBDIRS = \
    registeredQDbus \
#    PowerManagementDaemon  \
    ukui-power-manager-tray \
    ukui-upower \
    ukui-power-manager \
    plugin-power \
