/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTERYSAVING_H
#define BATTERYSAVING_H

#include "../common/common.h"
#include "powermanagercontrol.h"
#include "config.h"
#include "brightness.h"


enum batterySaverState
{
    batterySaverOff = 0,
    batterySaverOn,
    batterySaverUnavailable,
    batterySaverUnknow,
};

class BatterySaving : public QObject
{
    Q_OBJECT
public:
    explicit BatterySaving(QObject *parent = Q_NULLPTR);
    ~BatterySaving();
    void initConnect();

    void initBatterySavingState();

    void setBatterySaver(const int batterySaver);

    void setBatterySaverBrightness(const int);

    void dealBatterySaver(const int state);

private:
    QPointer<Config> m_config;

    QPointer<PowerManagerControl> m_control;

    QPointer<Brightness> m_brightness;

    int m_batterySaver;

    bool m_batterySaverBrightness;


private slots:
    void dealBatterySaverAuto(bool);

};

#endif // BATTERYSAVING_H
