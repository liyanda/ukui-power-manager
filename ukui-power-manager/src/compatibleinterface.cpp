#include "compatibleinterface.h"
#include <QtDebug>

#define BRIGHTNESS           "brightnessAc"
CompatibleInterface::CompatibleInterface(QObject *parent) : QObject(parent)
{


    const QByteArray brightSettings(BRIGHTNESS_SCHEMA);
    brightnesssettings = new QGSettings(brightSettings, QByteArray(), this);
    connect(brightnesssettings, &QGSettings::changed, [=](QString key) {
        if (BRIGHTNESS == key) {
            QDBusInterface iface("org.ukui.upower",
                                 "/upower/PowerManager",
                                 "org.ukui.powerManager",
                                 QDBusConnection::sessionBus());
            iface.call("SetBrightness",brightnesssettings->get(BRIGHTNESS).toInt());
            qDebug()<<""<<brightnesssettings->get(BRIGHTNESS).toInt();
        }
        });
}
