/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "batterysaving.h"

BatterySaving::BatterySaving(QObject *parent) : QObject(parent)
{
    m_config = Singleton<Config>::Instance();
    m_control = Singleton<PowerManagerControl>::Instance();
    m_brightness = Singleton<Brightness>::Instance();
}

BatterySaving::~BatterySaving() {}

void BatterySaving::initConnect()
{
    QDBusConnection::sessionBus().connect(
        UKUI_UPOWER_SERVICE,
        UKUI_UPOWER_BATTERY_PATH,
        UKUI_UPOWER_BATTERY_INTERFACE,
        "BatterySaverAuto",
        this,
        SLOT(dealBatterySaverAuto(bool)));
}

void BatterySaving::initBatterySavingState()
{
    m_batterySaver = m_config->getSysData().sysBatterySaver;
    setBatterySaverBrightness(m_config->getSysData().sysBatterySaverBrightness);
    if (batterySaverUnknow == m_batterySaver) {
        setBatterySaver(batterySaverOff);
    }
    if (unKnow != m_config->getSysData().sysPowerPolicyBackup) {
        m_control->setConfigValue(POWER_POLICY_BATTERY, m_config->getSysData().sysPowerPolicyBackup);
        m_control->setConfigValue(POWER_POLICY_BACKUP, unKnow);
    }
    QDBusInterface iface(
        UKUI_UPOWER_SERVICE, UKUI_UPOWER_BATTERY_PATH, UKUI_UPOWER_BATTERY_INTERFACE, QDBusConnection::sessionBus());
    QDBusReply<bool> reply = iface.call("BatteryAutoState");
    if (reply.isValid()) {
        dealBatterySaverAuto(reply.value());
    }
}

void BatterySaving::setBatterySaver(const int batterySaver)
{
    m_control->setConfigValue(BATTERY_SAVER, batterySaver);
    qDebug() << "set battery saver";
}

void BatterySaving::setBatterySaverBrightness(const int isState)
{
    if (m_batterySaverBrightness != isState) {
        m_batterySaverBrightness = isState;
    }
}

void BatterySaving::dealBatterySaverAuto(bool state)
{
    qDebug() << "battery saver auto state:" << state;
    if (state) {
        setBatterySaver(batterySaverOn);
    } else {
        if (batterySaverUnavailable != m_config->getSysData().sysBatterySaver) {
            setBatterySaver(batterySaverOff);
        }
    }
}

void BatterySaving::dealBatterySaver(const int state)
{
    if (m_batterySaver == state) {
        return;
    }
    bool isBrightness(true);
    int ret = state | m_batterySaver;
    if (2 == ret) {
        isBrightness = false;
        qDebug() << "No need to adjust brightness";
    }
    m_batterySaver = state;
    qDebug() << "Battery saver value is :" << m_batterySaver << m_batterySaverBrightness;
    if (batterySaverOn == m_batterySaver) {
        m_control->setConfigValue(POWER_POLICY_BACKUP, m_config->getSysData().sysPowerPolicyBat);
        m_control->setConfigValue(POWER_POLICY_BATTERY, energySaving);
        if (m_batterySaverBrightness) {
            m_brightness->reduceBrightness();
        }
    } else if (batterySaverOff == m_batterySaver || batterySaverUnavailable == m_batterySaver) {
        if (unKnow != m_config->getSysData().sysPowerPolicyBackup) {
            m_control->setConfigValue(POWER_POLICY_BATTERY, m_config->getSysData().sysPowerPolicyBackup);
            m_control->setConfigValue(POWER_POLICY_BACKUP, unKnow);
            if (m_batterySaverBrightness && isBrightness) {
                m_brightness->restoreBrightness();
            }
        }
    }
}
