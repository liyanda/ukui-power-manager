/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "idle.h"
#include "error.h"

Idle::Idle(QObject *parent)
    : QObject(parent)
    , m_turnOffDisplayTimer(new QTimer)
    , m_suspendTimer(new QTimer)
    , m_reduceBrightnessTimer(new QTimer)
{
    initIdle();
}

Idle::~Idle() {}

void Idle::initIdle()
{
    bool result = QDBusConnection::sessionBus().connect(
        SESSION_MANAGER_SERVICE,
        SESSION_MANAGER_PATH,
        GNOME_SESSION_MANAGER,
        "StatusChanged",
        this,
        SLOT(timerStart(quint32)));
    if (!result) {
        qCritical() << "error code: " << IDLE_SESSION_CONNECT_ERROR;
    }
    QDBusConnection::systemBus().connect(
        LOGIN1_SERVICE, LOGIN1_PATH, LOGIN1_MANAGER, "PrepareForSleep", this, SLOT(timerStop()));
    connect(m_turnOffDisplayTimer, &QTimer::timeout, this, [=]() {
        emit turnOffDisplayTimeout(SLEEP_DISPLAY);
        m_turnOffDisplayTimer->stop();
    });
    connect(m_suspendTimer, &QTimer::timeout, this, [=]() {
        emit suspendTimeout(SLEEP_COMPUTER);
        m_suspendTimer->stop();
    });
    connect(m_reduceBrightnessTimer, &QTimer::timeout, this, [=]() {
        emit reduceBrightnessTimeout(REDUCE_BRIGHTNESS);
        m_reduceBrightnessTimer->stop();
    });
}

void Idle::setIdleDisplayTime(const int &time)
{
    m_turnOffDisplayTime = time - m_SessionTime;
    qDebug() << "set turn off display time is :" << time;
}

void Idle::setIdleSuspendTime(const int &time)
{
    m_suspendTime = time - m_SessionTime;
    qDebug() << "set suspend time is :" << time;
}

void Idle::timerStart(quint32 data)
{
    if (m_SessionData == data) {
        int brightnessTime;
        emit idleChanged(ENTER_IDLE_STATE);
        qDebug() << m_turnOffDisplayTime << m_suspendTime;
        if (0 <= m_turnOffDisplayTime) {
            brightnessTime = m_turnOffDisplayTime - 10;
            m_turnOffDisplayTimer->start(m_turnOffDisplayTime * 1000);
            m_reduceBrightnessTimer->start(brightnessTime * 1000);
        } else {
            qDebug() << "TurnOffDisplayTimer not started!";
            if (0 < m_suspendTime) {
                brightnessTime = m_suspendTime - 10;
                m_reduceBrightnessTimer->start(brightnessTime * 1000);
            }
        }
        if (0 < m_suspendTime) {
            m_suspendTimer->start(m_suspendTime * 1000);
        } else {
            qDebug() << "SuspendTimer not started!";
        }
    } else if (0 == data) {
        timerStop();
        emit idleChanged(EXIT_IDLE_STATE);
    }
}

void Idle::timerStop()
{
    if (m_turnOffDisplayTimer->isActive()) {
        m_turnOffDisplayTimer->stop();
        qDebug() << "turn off display timer stop";
    }
    if (m_suspendTimer->isActive()) {
        m_suspendTimer->stop();
        qDebug() << "suspend timer stop";
    }
}
