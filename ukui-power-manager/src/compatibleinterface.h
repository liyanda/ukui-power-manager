#ifndef COMPATIBLEINTERFACE_H
#define COMPATIBLEINTERFACE_H

#include <QObject>
#include <QGSettings>
#include <QtDBus>

#define BRIGHTNESS_SCHEMA  "org.ukui.power-manager"

class CompatibleInterface : public QObject
{
    Q_OBJECT
public:
    explicit CompatibleInterface(QObject *parent = nullptr);

private:
    QGSettings *brightnesssettings;

signals:

};

#endif // COMPATIBLEINTERFACE_H
