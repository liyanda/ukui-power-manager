/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "powermanagercenter.h"
#include <QTranslator>
#include <QApplication>
#include <ukui-log4qt.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    initUkuiLog4qt("ukui-power-manager");
    QTranslator translator;
    if (translator.load(QLocale(), "ukui-power-manager", "_", QM_FILES_INSTALL_PATH)) {
        a.installTranslator(&translator);
    } else {
        qDebug() << "load ukui-power-manager-tray qm file error";
    }
    if (!QDBusConnection::sessionBus().registerService(QString("org.ukui.ukui-power-manager"))) {
        qCritical() << "ukui-power-manager has been activated";
        return 1;
    } else {
        PowerManagerCenter powermanagercenter;
        return a.exec();
    }
}
