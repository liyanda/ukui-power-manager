/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POWERMANAGERCENTER_H
#define POWERMANAGERCENTER_H

#include "../common/common.h"

class Config;
class Brightness;
class Lid;
class Idle;
class BatterySaving;
class Notification;
class PowerManagerControl;
class PowerPolicy;
class LowBattery;
class CompatibleInterface;


class PowerManagerCenter : public QObject
{
    Q_OBJECT
public:
    PowerManagerCenter();
    virtual ~PowerManagerCenter();

private:
    void initPowerManagerCenter();
    void initPowerModule(const int &type);
    void initPowerData(const int &type);
    void initPowerConnect(const int &type);
    void setIdleTime(const int &type);
    bool getOnBatteryState();
    void dealLidChanged(const bool &state);
    void dealIdleEvent(const QString &type);
    void dealConfigEvent(const QStringList &changedList);
    void dealSysDateChanged(const QString &type);
    void dealLowBatteryChanged(const int &state);
    void dealLowBatteryControl();
    QString transformData(const QString &data);
    QPointer<Config> m_config;
    QPointer<Brightness> m_brightness;
    QPointer<Lid> m_lid;
    QPointer<Idle> m_idle;
    QPointer<BatterySaving> m_batterySaving;
    QPointer<Notification> m_notification;
    QPointer<PowerManagerControl> m_control;
    QPointer<PowerPolicy> m_policy;
    QPointer<LowBattery> m_lowBattery;
    QPointer<CompatibleInterface> m_compatibleInterface;
    bool m_onBattery;
    bool m_isIdleBrightness=false;
private slots:
    void dealPowerStateChanged(bool state);
    void dealBatteryStateChanged(int state);
};
#endif // POWERMANAGERCENTER_H
