/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRIGHTNESS_H
#define BRIGHTNESS_H

#include "../common/common.h"
#include "powermanagercontrol.h"
#include "config.h"

class Brightness : public QObject
{
    Q_OBJECT
public:
    explicit Brightness(QObject *parent = Q_NULLPTR);
    ~Brightness();
    void setBrightness(const int brightness);

    void reduceBrightness();
    void restoreBrightness();


private:
    QPointer<PowerManagerControl> m_control;
    int m_brightness;

};

#endif // BRIGHTNESS_H
