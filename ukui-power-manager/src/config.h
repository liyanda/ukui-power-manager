/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "../common/common.h"
#include <QSettings>
#include <QFileSystemWatcher>

struct SysData
{
    int sysPowerPolicyAc;
    int sysPowerPolicyBat;
    int sysPowerPolicyBackup;
    int sysSleepDisplayAc;
    int sysSleepDisplayBat;
    int sysSleepComputerAc;
    int sysSleepComputerBat;
    int sysIdleDimTime;
    int sysLowBattery;
    int sysCriticalBattery;
    int sysBatterySaver;
    int sysBatterySaverPercent;
    int sysBrightness;
    bool sysLockBlankScreen;
    bool sysBatterySaverAuto;
    bool sysBatterySaverBrightness;
    QString sysActionCriticalBat;
    QString sysButtonLidAc;
    QString sysButtonLidBat;
};


enum powerPolicy
{
    performance = 0,
    balance,
    energySaving,
    unKnow,
};

class Config : public QObject
{
    Q_OBJECT
public:
    explicit Config(QObject *parent = Q_NULLPTR);
    ~Config();

    int machineType();
    SysData getSysData();
    void setBrightness(const double value);

signals:
    void configChanged(QStringList type);

private:
    bool isNeedConfig();
    void initConfig();
    void initConnect();
    void getConfData(const int &model);
    void updateConfData(const QString &key, const QVariant &value);
    QString getProduct();
    QStringList diffSysData();
    int getManufacturer();
    int vendorTypeMatch(const QString &type);
    int getMachineType();
    template<typename data>
    QString dataJudgment(const QString &type, data &D1, data &D2);

    QPointer<QSettings> m_settings;
    QPointer<QFileSystemWatcher> m_fileWatcher;
    QSharedPointer<QDBusInterface> m_iface;
    SysData m_sysData;
    int m_machineType;
};

#endif // CONFIG_H
