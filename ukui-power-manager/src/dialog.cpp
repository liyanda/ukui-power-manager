#include "dialog.h"
#include <QDebug>
#include <KWindowSystem>

Dialog::Dialog(QWidget *parent) : QDialog(parent)
{
    uiInit();
}

Dialog::~Dialog() {}

void Dialog::uiInit()
{
    //初始化弹窗界面
    setFixedSize(510, 224);
    setWindowFlag(Qt::WindowStaysOnTopHint);
    KWindowSystem::setState(this->winId(), NET::SkipTaskbar | NET::SkipPager);
    m_layout = new QVBoxLayout(this);
    m_topLayout = new QHBoxLayout(this);
    m_verLayout = new QVBoxLayout(this);
    m_verLayout->setContentsMargins(10, 10, 24, 24);

    m_icon = new QLabel(this);
    m_icon->setAlignment(Qt::AlignLeft);
    m_icon->setFixedSize(24, 24);
    m_icon->setPixmap(QIcon::fromTheme("ukui-power-manager").pixmap(QSize(24, 24)));

    m_title = new QLabel(this);
    m_title->setMargin(0);
    m_title->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_title->setMinimumWidth(1);
    m_title->setText(tr("ukui-power-manager"));

    m_closeButton = new QPushButton(this);
    m_closeButton->setFixedSize(32, 32);
    m_closeButton->setProperty("isWindowButton", 0x02);
    m_closeButton->setProperty("useIconHighlightEffect", 0x08);
    m_closeButton->setIcon(QIcon::fromTheme("window-close-symbolic"));
    m_closeButton->setFlat(true);

    //初始化消息提示
    m_tipsLabel = new QLabel(this);
    m_tipsLabel->setAlignment(Qt::AlignVCenter);
    m_tipsLabel->setMinimumWidth(1);
    m_tipsLabel->setFixedWidth(462);
    QString text = tr("The current battery level is very low. Please connect to the power supply as soon as possible "
                      "to prevent the "
                      "loss of files! The system will %1 automatically in 60 seconds......")
                       .arg(m_showText);
    m_tipsLabel->setText(text);
    m_tipsLabel->setWordWrap(true); //自动换行

    //初始化确认按钮
    m_confirmBtn = new QPushButton(this);
    m_confirmBtn->setFixedSize(112, 56);
    m_confirmBtn->setProperty("isImportant", true);
    m_confirmBtn->setText(tr("I know"));

    connect(m_closeButton, &QPushButton::clicked, this, &Dialog::confirmBtnClicked);
    connect(m_confirmBtn, &QPushButton::clicked, this, &Dialog::confirmBtnClicked);

    m_topLayout->addWidget(m_icon, 0, Qt::AlignLeft | Qt::AlignVCenter);
    m_topLayout->addWidget(m_title, 10, Qt::AlignLeft);
    m_topLayout->addWidget(m_closeButton, 0, Qt::AlignRight);

    m_verLayout->addWidget(m_tipsLabel, 0, Qt::AlignLeft | Qt::AlignCenter);
    m_verLayout->addWidget(m_confirmBtn, 0, Qt::AlignRight | Qt::AlignCenter);

    m_layout->addLayout(m_topLayout);
    m_layout->addLayout(m_verLayout);
    this->setLayout(m_layout);
}

void Dialog::timerInit(QString str)
{
    m_veryLowBatControl = str;
    m_showTimer = new QTimer;
    m_clockTimer = new QTimer;
    m_clockTimer->start(60000);
    m_showTimer->start(250);
    connect(m_showTimer, &QTimer::timeout, this, &Dialog::showTime);
    connect(m_clockTimer, &QTimer::timeout, this, &Dialog::stopTimer);
}

void Dialog::confirmBtnClicked()
{
    stopTimer();
    this->hide();
}

void Dialog::showTime()
{
    QString text
        = tr("The current battery level is very low. Please connect to the power supply as soon as possible to prevent "
             "the loss of files! The system will %1 automatically in %2 seconds......")
              .arg(m_showText)
              .arg(m_clockTimer->remainingTime() / 1000);
    m_tipsLabel->setText(text);
}

void Dialog::stopTimer()
{
    m_showTimer->stop();
    m_clockTimer->stop();
    this->hide();
    qDebug() << "dialog timer stop!";
}

void Dialog::setShowMsg(QString str)
{
    if (0 == str.compare("shutdown")) {
        m_showText = tr("shutdown");
    } else if (0 == str.compare("hibernate")) {
        m_showText = tr("hibernate");
    }
}

void Dialog::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    p.setPen(Qt::NoPen);
    QColor color = palette().color(QPalette::Base);
    QBrush brush = QBrush(color);
    p.setBrush(brush);

    p.setRenderHint(QPainter::Antialiasing);
    p.drawRect(opt.rect);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
