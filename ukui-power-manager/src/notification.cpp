/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "notification.h"

Notification::Notification(QObject *parent)
    : QObject(parent)
    , m_iface(new QDBusInterface(
          "org.freedesktop.Notifications",
          "/org/freedesktop/Notifications",
          "org.freedesktop.Notifications",
          QDBusConnection::sessionBus()))
{
}

Notification::~Notification() {}

void Notification::notifySend(const QString &type, const QString &arg, QString soundName)
{
    QList<QVariant> args;
    QStringList argg;
    QMap<QString, QVariant> pear_map;
    if (!soundName.isEmpty()) {
        pear_map.insert("sound-name", soundName);
    }
    args << tr("Power Manager") << ((unsigned int)0) << QString("ukui-power-manager")
         << type //显示的是什么类型的信息//系统升级
         << arg  //显示的具体信息
         << argg << pear_map << (int)-1;
    QDBusReply<uint> reply = m_iface->callWithArgumentList(QDBus::AutoDetect, "Notify", args);
    qDebug() << "msg code is ：" << reply.value();
}

void Notification::batteryStateMsgNotify(const int &state)
{
    QString type, msg;
    switch (state) {
        case Charging:
            type = tr("charge notification");
            msg = tr("battery is charging");
            notifySend(type, msg, "powerconnected");
            break;
        case Discharging:
            type = tr("discharged notification");
            msg = tr("battery is discharging");
            notifySend(type, msg, "mute");
            break;
        case FullyCharged:
            type = tr("full charge notification");
            msg = tr("battery is full");
            notifySend(type, msg, "mute");
            break;
        default:
            break;
    }
}

void Notification::lowBatteryNotify()
{
    QString type, msg;
    type = tr("Low battery notification");
    msg = tr("The system enters a low battery state");
    notifySend(type, msg, "lowpower");
}
