/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LID_H
#define LID_H

#include "../common/common.h"
#include <QDBusUnixFileDescriptor>

class Lid : public QObject
{
    Q_OBJECT
public:
    explicit Lid(QObject *parent = Q_NULLPTR);
    ~Lid();
    bool readLidState();

signals:
    void lidChanged(bool);

private:
    void inhibitSystemdLogin();
    void initLidConnection();

    QDBusUnixFileDescriptor logindLock;
    bool m_lidIsClosed;

private slots:
    void dealLidWatcherMssage(bool);
    void dealWakeUpSignal(bool);
};

#endif // LID_H
