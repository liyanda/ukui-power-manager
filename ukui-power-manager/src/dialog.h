#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QStyleOption>
#include <QPainter>
class Dialog : public QDialog
{
    Q_OBJECT
private:
    QVBoxLayout *m_layout = nullptr;
    QHBoxLayout *m_topLayout = nullptr;
    QVBoxLayout *m_verLayout = nullptr;
    QPushButton *m_confirmBtn = nullptr;
    QPushButton *m_closeButton = nullptr;
    QLabel *m_tipsLabel = nullptr;
    QLabel *m_icon = nullptr;
    QLabel *m_title = nullptr;
    QTimer *m_showTimer = nullptr;
    QTimer *m_clockTimer = nullptr;

    QString m_veryLowBatControl;
    QString m_showText;
    void uiInit();
protected slots:
    void confirmBtnClicked();
    void showTime();
    void stopTimer();

protected:
    void paintEvent(QPaintEvent *);

public:
    void timerInit(QString str);
    void setShowMsg(QString str);
    Dialog(QWidget *parent = nullptr);
    ~Dialog();
};

#endif // DIALOG_H
