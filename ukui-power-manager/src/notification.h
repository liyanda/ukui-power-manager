/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include "../common/common.h"

enum batteryState
{
    Unknow = 0,
    Charging,
    Discharging,
    Empty,
    FullyCharged,
    PendingCharged,
    PendingDischarged,
};

class Notification : public QObject
{
    Q_OBJECT
public:
    Notification(QObject *parent = Q_NULLPTR);
    ~Notification();
    void batteryStateMsgNotify(const int &state);
    void lowBatteryNotify();

private:
    void notifySend(const QString &type, const QString &arg, QString soundName);

    QSharedPointer<QDBusInterface> m_iface;

};

#endif // NOTIFICATION_H
