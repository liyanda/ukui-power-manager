/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IDLE_H
#define IDLE_H

#include <QTimer>
#include "../common/common.h"

#define REDUCE_BRIGHTNESS "reduceBrightness"
#define ENTER_IDLE_STATE  "enterIdleState"
#define EXIT_IDLE_STATE   "exitIdleState"

class Idle : public QObject
{
    Q_OBJECT
public:
    Idle(QObject *parent = Q_NULLPTR);
    ~Idle();
    void setIdleDisplayTime(const int &time);
    void setIdleSuspendTime(const int &time);

public slots:
    void timerStop();

signals:
    void suspendTimeout(QString type);
    void turnOffDisplayTimeout(QString type);
    void reduceBrightnessTimeout(QString type);
    void idleChanged(QString type);
private:
    void initIdle();
    int m_turnOffDisplayTime;
    int m_suspendTime;
    QPointer<QTimer> m_turnOffDisplayTimer;
    QPointer<QTimer> m_suspendTimer;
    QPointer<QTimer> m_reduceBrightnessTimer;

    const quint32 m_SessionData = 3;
    const int m_SessionTime = 60;

private slots:
    void timerStart(quint32);

};

#endif // IDLE_H
