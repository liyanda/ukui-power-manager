/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "brightness.h"

Brightness::Brightness(QObject *parent) : QObject(parent)
{
    m_control = Singleton<PowerManagerControl>::Instance();
    qDebug() << "Brightness" << SUCCESS;
}

Brightness::~Brightness() {}

void Brightness::setBrightness(const int brightness)
{
    if (m_brightness != brightness) {
        m_brightness = brightness;
        qDebug() << "brightness chaned, value :" << m_brightness;
        m_control->setBrigtness(m_brightness);
    }
}

void Brightness::reduceBrightness()
{
    int brightness = m_brightness * 0.7;
    m_control->setConfigValue(BRIGHTNESS, brightness);
    qDebug() << "reduce brightness";
}

void Brightness::restoreBrightness()
{
    int brightness = m_brightness / 0.7;
    if (brightness > 100) {
        brightness = 100;
    }
    m_control->setConfigValue(BRIGHTNESS, brightness);
    qDebug() << "restore brightness";
}
