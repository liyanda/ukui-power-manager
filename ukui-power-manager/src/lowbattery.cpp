/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <KWindowEffects>
#include <QApplication>
#include <QDesktopWidget>
#include "lowbattery.h"
#include "../common/xatom-helper.h"

LowBattery::LowBattery(QObject *parent)
    : QObject(parent)
    , m_controlTimer(new QTimer)
    , m_iface(new QDBusInterface(
          UKUI_UPOWER_SERVICE, UKUI_UPOWER_BATTERY_PATH, UKUI_UPOWER_BATTERY_INTERFACE, QDBusConnection::sessionBus()))
{
    m_config = Singleton<Config>::Instance();
    initLowBattery();
    initConnect();
}

LowBattery::~LowBattery() {}

void LowBattery::initLowBattery()
{
    qDebug() << "low battery init ";
    if (Critical == getBatteryData("WarningLevel")) {
        m_lowBatteryState = Critical;
    } else if (Low == getBatteryData("LowBattery")) {
        m_lowBatteryState = Low;
    } else {
        m_lowBatteryState = UnKnow;
    }
    qDebug() << "low battery state :" << m_lowBatteryState;
}

void LowBattery::initConnect()
{
    QDBusConnection::sessionBus().connect(
        UKUI_UPOWER_SERVICE,
        UKUI_UPOWER_BATTERY_PATH,
        UKUI_UPOWER_BATTERY_INTERFACE,
        "LowBatteryState",
        this,
        SLOT(dealLowBatteryMssage(int)));
    connect(m_controlTimer, &QTimer::timeout, this, [=](){
       veryLowBatteryControlStop();
       emit veryLowBatteryTimeout();
    });
}

void LowBattery::veryLowBatteryDialog(const QString msg)
{
    if (m_controlTimer->isActive()) {
        return;
    }
    m_dialog = QPointer<Dialog>(new Dialog);
    setDialogMsg();
    KWindowEffects::enableBlurBehind(m_dialog->winId(), true);
    // 添加窗管协议
    MotifWmHints hints;
    hints.flags = MWM_HINTS_FUNCTIONS | MWM_HINTS_DECORATIONS;
    hints.functions = MWM_FUNC_ALL;
    hints.decorations = MWM_DECOR_BORDER;
    XAtomHelper::getInstance()->setWindowMotifHint(m_dialog->winId(), hints);

    m_dialog->show();
    m_dialog->move(
        (QApplication::desktop()->width() - m_dialog->width()) / 2,
        (QApplication::desktop()->height() - m_dialog->height()) / 2);
    m_dialog->timerInit(msg);
    m_controlTimer->start(60000);
}

void LowBattery::veryLowBatteryControlStop()
{
    if (m_controlTimer->isActive()) {
        m_controlTimer->stop();
        m_dialog->hide();
        delete m_dialog;
        qDebug() << "stop low battery timer success";
    }
}

void LowBattery::dealLowBatteryMssage(int state)
{
    if (state != m_lowBatteryState) {
        m_lowBatteryState = state;
        qDebug() << "low battery state changed" << state;
        emit lowBatteryStateChanged(m_lowBatteryState);
    }
}

int LowBattery::getBatteryData(const QString method)
{
    QDBusReply<int> reply = m_iface->call(method);
    if (reply.isValid()) {
        return reply.value();
    } else {
        qCritical() << "error code: " << GET_BATTERY_DATA_ERROR;
        return 0;
    }
}

int LowBattery::getLowBatteryWarning()
{
    return m_lowBatteryState;
}

void LowBattery::setDialogMsg()
{
    m_dialog->setShowMsg(m_config->getSysData().sysActionCriticalBat);
}
