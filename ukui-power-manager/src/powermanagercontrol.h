/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POWERMANAGERCONTROL_H
#define POWERMANAGERCONTROL_H

#include "../common/common.h"
#include <QProcess>

class PowerManagerControl : public QObject
{
    Q_OBJECT
public:
    PowerManagerControl(QObject *parent = Q_NULLPTR);
    ~PowerManagerControl();
    void dealControlAction(const QString type, const QVariant data);
    void setBrigtness(const int value);
    void setConfigValue(const QString type, const QVariant value);
    int getBrightness();

private:
    QPointer<QDBusInterface> m_iface;
    template<typename key, typename value>
    void controlAction(const key &type, const value &data);
    template<typename key, typename value, typename value1>
    void controlAction(const key &type, const value &data, const value1 &data1);
    QString executeLinuxCmd(const QString strCmd);
    void controlDisplay(const QString state);
    void turnOffDisplaySignal();
};

#endif // POWERMANAGERCONTROL_H
