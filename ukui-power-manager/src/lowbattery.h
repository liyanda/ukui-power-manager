/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOWBATTERY_H
#define LOWBATTERY_H

#include <QTimer>
#include "../common/common.h"
#include "config.h"
#include "dialog.h"

enum warningLevel
{
    UnKnow = 0,
    None,
    DischargingOnlyUPS,
    Low,
    Critical,
    Action,
};

class LowBattery : public QObject
{
    Q_OBJECT
public:
    LowBattery(QObject *parent = Q_NULLPTR);
    ~LowBattery();

    int getLowBatteryWarning();

    void veryLowBatteryDialog(const QString msg);

    void veryLowBatteryControlStop();

    void setDialogMsg();
signals:
    void lowBatteryStateChanged(int);

    void veryLowBatteryTimeout();

private:
    void initConnect();

    void initLowBattery();

    int getBatteryData(const QString method);

    QPointer<QTimer> m_controlTimer;

    QPointer<Config> m_config;

    QPointer<QDBusInterface> m_iface;

    QPointer<Dialog> m_dialog;

    int m_lowBatteryState;

private slots:
    void dealLowBatteryMssage(int);

};

#endif // LOWBATTERY_H
