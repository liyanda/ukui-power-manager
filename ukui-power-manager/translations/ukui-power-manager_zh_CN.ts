<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../src/dialog.cpp" line="32"/>
        <source>ukui-power-manager</source>
        <translation>电源管理</translation>
    </message>
    <message>
        <source>The current battery level is very low. Please connect to the power supply as soon as possible to prevent the loss of files! The system will 1% automatically in 60 seconds......</source>
        <translation type="vanished">当前电池电量极低，请尽快接入电源，防止文件丢失！系统将在60秒后自动 1%......</translation>
    </message>
    <message>
        <location filename="../src/dialog.cpp" line="57"/>
        <source>I know</source>
        <translation>知道了</translation>
    </message>
    <message>
        <source>The current battery level is very low. Please connect to the power supply as soon as possible to prevent the loss of files! The system will 1% automatically in 2% seconds......</source>
        <translation type="vanished">当前电池电量极低，请尽快接入电源，防止文件丢失！系统将在 2% 秒后自动 1%  ......</translation>
    </message>
    <message>
        <source>The current battery level is very low. Please connect to the power supply as soon as possible to prevent the loss of files! The system will %1 automatically in 2% seconds......</source>
        <translation type="vanished">当前电池电量极低，请尽快接入电源，防止文件丢失！系统将在2%秒后自动1% ......</translation>
    </message>
    <message>
        <location filename="../src/dialog.cpp" line="46"/>
        <source>The current battery level is very low. Please connect to the power supply as soon as possible to prevent the loss of files! The system will %1 automatically in 60 seconds......</source>
        <translation>当前电池电量极低，请尽快接入电源，防止文件丢失！系统将在60秒后自动%1......</translation>
    </message>
    <message>
        <location filename="../src/dialog.cpp" line="94"/>
        <source>The current battery level is very low. Please connect to the power supply as soon as possible to prevent the loss of files! The system will %1 automatically in %2 seconds......</source>
        <translation>当前电池电量极低，请尽快接入电源，防止文件丢失！系统将在 %2 秒后自动%1  ......</translation>
    </message>
    <message>
        <location filename="../src/dialog.cpp" line="112"/>
        <source>shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../src/dialog.cpp" line="114"/>
        <source>hibernate</source>
        <translation>休眠</translation>
    </message>
</context>
<context>
    <name>Notification</name>
    <message>
        <location filename="../src/notification.cpp" line="40"/>
        <source>Power Manager</source>
        <translation>电源管理</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="53"/>
        <source>charge notification</source>
        <translation>充电通知</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="54"/>
        <source>battery is charging</source>
        <translation>电池正在充电</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="58"/>
        <source>discharged notification</source>
        <translation>放电通知</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="59"/>
        <source>battery is discharging</source>
        <translation>电池正在放电中</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="63"/>
        <source>full charge notification</source>
        <translation>充满电通知</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="64"/>
        <source>battery is full</source>
        <translation>电池现在已经充满</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="75"/>
        <source>Low battery notification</source>
        <translation>低电量消息通知</translation>
    </message>
    <message>
        <location filename="../src/notification.cpp" line="76"/>
        <source>The system enters a low battery state</source>
        <translation>系统进入低电量状态</translation>
    </message>
</context>
</TS>
