QT += widgets dbus

TEMPLATE = lib
CONFIG += plugin \
          link_pkgconfig \
          c++11

LIBS += -lukcc

PKGCONFIG += gsettings-qt\
             kysdk-qtwidgets \
             kysdk-sysinfo

TARGET = $$qtLibraryTarget(power)
target.path = $$[QT_INSTALL_LIBS]/ukui-control-center


TRANSLATIONS += translations/zh_CN.ts

isEmpty(PREFIX) {
    PREFIX = /usr
}

qm_files.files = translations/*
qm_files.path = /usr/share/ukui-power-manager/power-plugin/translations

ts_files.files = translations/*
ts_files.path = /usr/share/ukui-power-manager/power-plugin/translations

CONFIG(release, debug|release) {
    !system($$PWD/translate_generation.sh): error("Failed to generate translation")
}


INSTALLS += target \
            qm_files \
	    ts_files \

HEADERS += \
    power.h \
    powermacrodata.h

SOURCES += \
    power.cpp
