/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * Copyright (C) 2019 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */
#ifndef POWERMACRODATA_H
#define POWERMACRODATA_H

#define ENERGYSAVINGMODE  "energysavingmode"
#define IDLE_DIM_AC "idle-dim-ac"
#define IDLE_DIM_BA "idle-dim-battery"

#define POWERMANAGER_SCHEMA     "org.ukui.power-manager"
#define ICONPOLICY              "icon-policy"
#define BUTTON_SUSPEND_KEY      "button-suspend"
#define BUTTON_POWER_KEY        "button-power"
#define HIBERNATE_KEY           "after-idle-action"
#define PER_ACTION_KEY          "percentage-action"

#define UKUI_UPOWER_SERVICE                 "org.ukui.upower"
#define UKUI_UPOWER_PATH                    "/upower/PowerManager"
#define UKUI_UPOWER_INTERFACE               "org.ukui.powerManager"
#define POWER_MANAGEMENT_SERVICE            "org.ukui.powermanagement"
#define POWER_MANAGEMENT_PATH               "/"
#define POWER_MANAGEMENT_INTERFACE_NAME     "org.ukui.powermanagement.interface"

#define INIT_POWER_MANAGER       "initPowerManager"              //电源管理初始化状态
#define SLEEP_DISPLAY            "sleepDisplay"
#define SLEEP_DISPLAY_AC_KEY     "sleepDisplayAc"
#define SLEEP_DISPLAY_BAT_KEY    "sleepDisplayBattery"        //空闲关闭显示器
#define SLEEP_COMPUTER           "sleepComputer"
#define SLEEP_COMPUTER_AC        "sleepComputerAc"
#define SLEEP_COMPUTER_BATTERY   "sleepComputerBattery"          //空闲随睡
#define LOCK_BLANK_SCREEN        "lockBlankScreen"
#define BRIGHTNESS               "brightness"                    //亮度值
#define BUTTON_LID_AC            "buttonLidAc"                   //盒盖事件触发操作
#define BUTTON_LID_BATTERY       "buttonLidBattery"
#define POWER_POLICY_AC          "powerPolicyAc"                 //电源策略(0:性能 1:平衡 2:节能)
#define POWER_POLICY_BATTERY     "powerPolicyBattery"            //电池策略
#define POWER_POLICY_BACKUP      "powerPolicyBackup"             //电池策略备份
#define ON_BATTERY_AUTO_SAVE     "onBatteryAutoSave"             //电池自动开启节能模式
#define LOW_BATTERY_ATUO_SAVE    "lowBatteryAutoSave"            //低电量时自动开启节能模式
#define LOW_BATTERY              "lowBattery"                    //低电量百分百通知
#define CRITICAL_BATTERY         "criticalBattery"               //极低电量
#define ACTION_CRITICAL_BATTERY  "actionCriticalBattery"         //极低电量时执行
#define BATTERY_SAVER            "batterySaver"                  //节电模式
#define BATTERY_SAVER_AUTO       "batterySaverAuto"              //自动开启节电模式
#define BATTERY_SAVER_BRIGHTNESS "batterySaverBrightness"        //节电模式亮度调节功能
#define BATTERY_SAVER_PERCENT    "batterySaverPercent"           //自动开启节电模式百分比

#define SCREENSAVER_SCHEMA       "org.ukui.screensaver"
#define SLEEP_ACTIVATION_ENABLED "sleep-activation-enabled"
#define CLOSE_ACTIVATION_ENABLED "close-activation-enabled"
#define SCREENLOCK_LOCK_KEY      "lock-enabled"
#define SCREENLOCK_ACTIVE_KEY    "idle-activation-enabled"

#define SESSION_SCHEMA          "org.ukui.session"
#define IDLE_DELAY_KEY          "idle-delay"

#define FIXES 60

#define PERSONALSIE_SCHEMA     "org.ukui.control-center.personalise"
#define PERSONALSIE_POWER_KEY  "custompower"
#define ISWHICHCHECKED         "ischecked"
#define POWER_MODE             "power-mode"

#define STYLE_FONT_SCHEMA  "org.ukui.style"
#define POWER_SCHEMA  "org.ukui.power-manager"
#endif // POWERMACRODATA_H
