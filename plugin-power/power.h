#ifndef UKCCPLUGIN_H
#define UKCCPLUGIN_H

#include <QObject>
#include <QtPlugin>
#include <QStyledItemDelegate>
#include <QGSettings>
#include <QWidget>
#include <QTranslator>
#include <QIcon>
#include <QLocale>
#include <QApplication>
#include <QDBusInterface>

#include <ukcc/interface/interface.h>
#include <ukcc/widgets/titlelabel.h>
#include <ukcc/widgets/comboxframe.h>
#include <ukcc/widgets/fixlabel.h>
#include "kswitchbutton.h"

using namespace kdk;

struct dataType
{
    const QString Str = "";
    const int Int = -101;
    const bool Bool = true;
};

class Power : public QObject, CommonInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.ukcc.CommonInterface")
    Q_INTERFACES(CommonInterface)

public:

    explicit Power();
    ~Power();

    QString plugini18nName()        Q_DECL_OVERRIDE;
    int pluginTypes()               Q_DECL_OVERRIDE;
    QWidget *pluginUi()             Q_DECL_OVERRIDE;
    bool isEnable() const           Q_DECL_OVERRIDE;
    const QString name() const      Q_DECL_OVERRIDE;
    bool isShowOnHomePage() const   Q_DECL_OVERRIDE;
    QIcon icon() const              Q_DECL_OVERRIDE;
    QString translationPath() const Q_DECL_OVERRIDE;

public:
    void InitUI(QWidget *widget);
    void retranslateUi();
    void resetui();
    void initSearchText();      // 搜索翻译
    /**
     * @brief batterySavingWidget
     * 电池节电计划界面
     */
    void batterySavingWidget();
    /**
     * @brief powerPlan
     * 电源计划
     */
    void powerPlan();
    void setupComponent();
    void setupConnect();
    void isLidPresent();
    void isHibernateSupply();
    bool isExitBattery();
    bool QLabelSetText(QLabel *label, QString string);
    void clearAutoItem(QVBoxLayout *mLyt);
    void setFrame_Noframe(QFrame *frame);


    QFrame *setLine(QFrame *frame);

public slots:
    void initCustomPlanStatus(QStringList list);

private:
    QGSettings *screensettings;
    QGSettings *stylesettings;
    QGSettings *sessionsettings;
    QGSettings *settings;

    bool mFirstLoad;
    QDBusInterface *m_iface;
    QDBusInterface *m_sysIface;

    QString pluginName;
    int pluginType;
    QWidget *widget;
    TitleLabel *CustomTitleLabel;
    TitleLabel *PowerPlanTitleLabel;
    TitleLabel *BatteryPlanTitleLabel;
    TitleLabel *BatterySavePlanTitleLabel;
    QLabel *mSleepPwdLabel;
    QLabel *mWakenPwdLabel;
    QLabel *mPowerKeyLabel;
    QLabel *mCloseLabel;
    QLabel *mSleepLabel;
    QLabel *mCloseLidLabel;
    QLabel *mPowerLabel;
    QLabel *mBatteryLabel;
    QLabel *mNoticeLabel;
    QLabel *mDisplayTimeLabel;
    QLabel *mBatterySaveLevel;
    QLabel *mAutoBrightnessLevel;
    QLabel *mLowBatteryAutoSavingLebel;

    QFrame *Powerwidget;
    QFrame *PowerPlanwidget;
    QFrame *Batterywidget;
    QFrame *mSleepPwdFrame;
    QFrame *mWakenPwdFrame;
    QFrame *mPowerKeyFrame;
    QFrame *mCloseFrame;
    QFrame *mSleepFrame;
    QFrame *mCloseLidFrame;
    QFrame *mPowerFrame;
    QFrame *mBatteryFrame;
    QFrame *mNoticeLFrame;
    //开启节电计划
    QFrame *mOpenBatterySaveFrame;
    QFrame *mAutoBrightnessFrame;

    QFrame *line_1;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_4;
    QFrame *line_5;
    QFrame *line_6;
    QFrame *line_7;
    QFrame *line_9;

    QSpacerItem *mItem;

    QComboBox *mPowerKeyComboBox;
    QComboBox *mCloseComboBox;
    QComboBox *mSleepComboBox;
    QComboBox *mCloseLidComboBox;
    QComboBox *mPowerComboBox;
    QComboBox *mBatteryComboBox;
    QComboBox *mDarkenComboBox;
    QComboBox *mNoticeComboBox;

    KSwitchButton *mSleepPwdBtn;
    KSwitchButton *mWakenPwdBtn;
    //开启节电模式
    KSwitchButton *mOpenBatterySavingBtn;
    KSwitchButton *mAutoBrightnessBtn;
    KSwitchButton *mLowBatteryAutoSavingBtn;

    QButtonGroup *mPowerBtnGroup;

    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_1;
    QSpacerItem *verticalSpacer_2;

    QStringList buttonStringList;
    QStringList sleepStringList;
    QStringList closeStringList;
    QStringList closeLidStringList;
    QStringList PowerplanStringList;
    QStringList BatteryplanStringList;
    QStringList DarkenStringList;
    QStringList LowpowerStringList;

    QStringList mKeys;

    QVBoxLayout *BatteryLayout;
    QVBoxLayout *PowerLayout;

    bool isExitsLid;
    bool isExitHibernate;
    bool hasBat;
    dataType t1;

    template<typename data>
    data getPowerConfData(const QString name,data type);
    void setConfigValue(const QString key, const QVariant value);
};

#endif // UKCCPLUGIN_H
