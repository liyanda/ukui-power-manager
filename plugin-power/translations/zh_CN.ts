<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Power</name>
    <message>
        <location filename="../power.cpp" line="24"/>
        <source>Power</source>
        <translation>电源</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="348"/>
        <source>Open BatterySaving</source>
        <translation>开启节电计划</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="377"/>
        <source>Auto Display BrightnessLevel</source>
        <translation>节电模式下自动降低屏幕亮度</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="402"/>
        <source>Battery Lower than</source>
        <translation>电量低于</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="410"/>
        <source>Open AutoSaving Mode</source>
        <translation>时自动开启节电计划</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="428"/>
        <source>Require Password when sleep</source>
        <translation>从睡眠中唤醒需要密码</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="429"/>
        <source>Require password when sleep</source>
        <translation>从睡眠中唤醒需要密码</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="432"/>
        <location filename="../power.cpp" line="433"/>
        <source>Password required when waking up the screen</source>
        <translation>唤醒屏幕时需要密码</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="436"/>
        <source>Press the power button</source>
        <translation>按下电源键执行</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="440"/>
        <location filename="../power.cpp" line="441"/>
        <source>Time to close display</source>
        <translation>此时间段后关闭显示器</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="444"/>
        <location filename="../power.cpp" line="445"/>
        <source>Time to sleep</source>
        <translation>此时间段后进入睡眠</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="448"/>
        <location filename="../power.cpp" line="449"/>
        <source>Notebook cover</source>
        <translation>合上盖子时执行</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="452"/>
        <location filename="../power.cpp" line="453"/>
        <location filename="../power.cpp" line="457"/>
        <source>Using power</source>
        <translation>使用电源</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="456"/>
        <source>Using battery</source>
        <translation>使用电池</translation>
    </message>
    <message>
        <source> Time to darken</source>
        <translation type="vanished">时间</translation>
    </message>
    <message>
        <source>Battery level is lower than</source>
        <translation type="vanished">电源低于</translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="vanished">运行</translation>
    </message>
    <message>
        <source>Automatically run saving mode when low battery</source>
        <translation type="vanished">极低电量时执行</translation>
    </message>
    <message>
        <source>Automatically run saving mode when the low battery</source>
        <translation type="vanished">极低电量时执行</translation>
    </message>
    <message>
        <source>Automatically run saving mode when using battery</source>
        <translation type="vanished">极低电量时执行</translation>
    </message>
    <message>
        <source>Display remaining charging time and usage time</source>
        <translation type="vanished">显示剩余充放电时间</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="483"/>
        <source>General</source>
        <translation>通用</translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../power.cpp" line="485"/>
        <source>Select Powerplan</source>
        <translation>电源计划</translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../power.cpp" line="487"/>
        <source>Battery saving plan</source>
        <translation>节电计划</translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../power.cpp" line="493"/>
        <source>nothing</source>
        <translation>无操作</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="493"/>
        <source>blank</source>
        <translation>锁屏</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="493"/>
        <location filename="../power.cpp" line="504"/>
        <source>suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="493"/>
        <location filename="../power.cpp" line="504"/>
        <location filename="../power.cpp" line="543"/>
        <source>shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="499"/>
        <location filename="../power.cpp" line="504"/>
        <source>hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="504"/>
        <source>interactive</source>
        <translation>询问</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <source>5min</source>
        <translation>5分钟</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>10min</source>
        <translation>10分钟</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>15min</source>
        <translation>15分钟</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>30min</source>
        <translation>30分钟</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>1h</source>
        <translation>1小时</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>2h</source>
        <translation>2小时</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="513"/>
        <location filename="../power.cpp" line="523"/>
        <source>never</source>
        <translation>从不</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="523"/>
        <source>3h</source>
        <translation>3小时</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="533"/>
        <location filename="../power.cpp" line="538"/>
        <source>Balance Model</source>
        <translation>平衡模式</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="533"/>
        <location filename="../power.cpp" line="538"/>
        <source>Save Model</source>
        <translation>节能模式</translation>
    </message>
    <message>
        <location filename="../power.cpp" line="533"/>
        <location filename="../power.cpp" line="538"/>
        <source>Performance Model</source>
        <translation>性能模式</translation>
    </message>
</context>
<context>
    <name>UkccPlugin</name>
    <message>
        <source>UkccPlugin</source>
        <translation type="vanished">测试插件</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <source>ukccplugin test</source>
        <translation type="vanished">插件翻译测试</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
</TS>
