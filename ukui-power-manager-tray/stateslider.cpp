#include "stateslider.h"

//滑动条组件
StateSlider::StateSlider(QWidget *parent) : KSlider(parent)
{
    setOrientation(Qt::Orientation::Horizontal);
    setSliderType(KSliderType::SingleSelectSlider);
    setMaximum(2);
    setSingleStep(1);
    setPageStep(1);
    setFixedWidth(372);
}

StateSlider::~StateSlider() {}

void StateSlider::mousePressEvent(QMouseEvent *event)
{
    int currentX = event->pos().x();
    double per = currentX * 1.0 / this->width();
    int value = per * (this->maximum() - this->minimum()) + this->minimum();
    this->setValue(value);
    QSlider::mousePressEvent(event);
}

void StateSlider::setSliderValue()
{
#if 0
    int policy = -1;
    if (1 == m_batteryState || 4 == m_batteryState || 5 == m_batteryState) {
        policy = m_PowerManagerGsettings->get(POWER_POLICY_AC).toInt();
    } else {
        policy = m_PowerManagerGsettings->get(POWER_POLICY_BATTERY).toInt();
    }
    switch (policy) {
        case Performance:
            this->setValue(2);
            break;
        case Balance:
            this->setValue(1);
            break;
        case EnergySaving:
            this->setValue(0);
            break;
        default:
            break;
    }
#endif
}
void StateSlider::sliderValueChanged(int value)
{
#if 0
    qDebug() << "slider value" << value << m_batteryState;
    if (1 == m_batteryState || 4 == m_batteryState || 5 == m_batteryState) {
        switch (value) {
            case 0:
                m_PowerManagerGsettings->set(POWER_POLICY_AC, EnergySaving);
                break;
            case 1:
                m_PowerManagerGsettings->set(POWER_POLICY_AC, Balance);
                break;
            case 2:
                m_PowerManagerGsettings->set(POWER_POLICY_AC, Performance);
                break;
            default:
                break;
        }
    } else {
        switch (value) {
            case 0:
                m_PowerManagerGsettings->set(POWER_POLICY_BATTERY, EnergySaving);
                break;
            case 1:
                m_PowerManagerGsettings->set(POWER_POLICY_BATTERY, Balance);
                break;
            case 2:
                m_PowerManagerGsettings->set(POWER_POLICY_BATTERY, Performance);
                break;
            default:
                break;
        }
    }
#endif
}
