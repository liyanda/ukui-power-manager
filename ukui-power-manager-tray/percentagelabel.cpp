#include "percentagelabel.h"

#include <QFont>
#include <QDBusReply>
#include <QtDBus>

#define ORG_UKUI_STYLE "org.ukui.style"
#define STYLE_NAME "styleName"
#define SYSTEM_FONT_SIZE "systemFontSize"

PercentageLabel::PercentageLabel()
{
    setFontsize();
    watchGsetting();
}

void PercentageLabel::setFontsize()
{
    qDebug() << "m_sysFontSize:" << m_sysFontSize;
    ft.setPointSize(m_sysFontSize * 2);
    this->setFont(ft);
}

void PercentageLabel::watchGsetting()
{
    const QByteArray styleId(ORG_UKUI_STYLE);
    if (QGSettings::isSchemaInstalled(styleId)) {
        QGSettings *styleSettings = new QGSettings(styleId);
        connect(styleSettings, &QGSettings::changed, this, [=](const QString &key) {
            if (key == SYSTEM_FONT_SIZE) {
                m_sysFontSize = styleSettings->get(SYSTEM_FONT_SIZE).toInt();
                setFontsize();
            }
        });
        m_sysFontSize = styleSettings->get(SYSTEM_FONT_SIZE).toInt();
    }
}
