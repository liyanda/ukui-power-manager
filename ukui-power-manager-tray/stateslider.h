#ifndef STATESLIDER_H
#define STATESLIDER_H
#include <QSlider>
#include <QDebug>
#include <QWidget>
#include <QMouseEvent>

//#include "ktabbar.h"
#include "kslider.h"
//#include "kwidget.h"

using namespace kdk;

class StateSlider : public KSlider
{
    Q_OBJECT
public:
    StateSlider(QWidget *parent = 0);
    ~StateSlider();
    void mousePressEvent(QMouseEvent *event);

public Q_SLOTS:
    void sliderValueChanged(int);

private:
    void setSliderValue();

};

#endif // STATESLIDER_H
