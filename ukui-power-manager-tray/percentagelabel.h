#ifndef PERCENTAGELABEL_H
#define PERCENTAGELABEL_H

#include <QObject>
#include <QLabel>
#include <QGSettings>

class PercentageLabel : public QLabel
{
    Q_OBJECT
public:
    explicit PercentageLabel();

private:
    void setFontsize();
    void watchGsetting();

    QFont ft;
    int  m_sysFontSize = 12;
Q_SIGNALS:

};

#endif // PERCENTAGELABEL_H
