<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DeviceForm</name>
    <message>
        <source>RemainTime</source>
        <translation type="vanished">预计剩余时间</translation>
    </message>
</context>
<context>
    <name>DeviceWidget</name>
    <message>
        <source>RemainTime</source>
        <translation type="vanished">预计剩余时间</translation>
    </message>
</context>
<context>
    <name>EngineDevice</name>
    <message>
        <location filename="../enginedevice.cpp" line="101"/>
        <source>yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="101"/>
        <source>no</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="120"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="120"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <source>Unknown time</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="vanished">分钟</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="vanished">分钟</translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="vanished">小时</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="vanished">小时</translation>
    </message>
    <message>
        <source>fully charged</source>
        <translation type="vanished">已充满</translation>
    </message>
    <message>
        <source>not charging</source>
        <translation type="vanished">未充电</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="281"/>
        <source>%1% available, charged</source>
        <translation>%1%可用，已充满</translation>
    </message>
    <message>
        <source>%1% available, not charging</source>
        <translation type="vanished">%1%可用，未充电</translation>
    </message>
    <message>
        <source>%1% available, charging</source>
        <translation type="vanished">%1%可用，正在充电</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="306"/>
        <source>%1 waiting to discharge (%2%)</source>
        <translation>%1 等待放电 (%2%)</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="311"/>
        <source>%1 waiting to charge (%2%)</source>
        <translation>%1 等待充电 (%2%)</translation>
    </message>
    <message>
        <source>discharging(%1%)</source>
        <translation type="vanished">放电(%1%)</translation>
    </message>
    <message>
        <source>charging(%1%)</source>
        <translation type="vanished">充电(%1%)</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="331"/>
        <source>AC adapter</source>
        <translation>交流电源适配器</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="335"/>
        <source>Laptop battery</source>
        <translation>笔记本电池</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="339"/>
        <source>UPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="343"/>
        <source>Monitor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="347"/>
        <source>Mouse</source>
        <translation>鼠标</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="351"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="355"/>
        <source>PDA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="359"/>
        <source>Cell phone</source>
        <translation>手机</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="363"/>
        <source>Media player</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="367"/>
        <source>Tablet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="371"/>
        <source>Computer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="375"/>
        <source>unrecognised</source>
        <translation>未识别</translation>
    </message>
    <message>
        <source>charging</source>
        <translation type="vanished">正在充电</translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="287"/>
        <source>Left %1h %2m (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="292"/>
        <source>%1% available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="298"/>
        <source>Left %1h %2m to full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="300"/>
        <source>charging (%1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>discharging</source>
        <translation type="vanished">放电</translation>
    </message>
    <message>
        <source>empty</source>
        <translation type="vanished">电量空</translation>
    </message>
    <message>
        <source>fully</source>
        <translation type="vanished">电量满</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="vanished">其他</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>SetPower</source>
        <translation type="vanished">设置电源和睡眠</translation>
    </message>
    <message>
        <source>ShowPercentage</source>
        <translation type="vanished">显示百分比</translation>
    </message>
    <message>
        <source>SetBrightness</source>
        <translation type="vanished">调整亮度</translation>
    </message>
    <message>
        <source>discharge notify notification</source>
        <translation type="vanished">正在放电</translation>
    </message>
    <message>
        <source>battery is discharging!</source>
        <translation type="vanished">电池正在放电</translation>
    </message>
    <message>
        <source>fullly charged notification</source>
        <translation type="vanished">电池充满</translation>
    </message>
    <message>
        <source>battery is fullly charged!</source>
        <translation type="vanished">电池已经充满</translation>
    </message>
    <message>
        <source>low battery notification</source>
        <translation type="vanished">电量低</translation>
    </message>
    <message>
        <source>battery is low,please plug in!</source>
        <translation type="vanished">电量低，请充电</translation>
    </message>
    <message>
        <source>critical battery notification</source>
        <translation type="vanished">电量很低</translation>
    </message>
    <message>
        <source>battery is critical low,please plug in!</source>
        <translation type="vanished">电量很低，请充电</translation>
    </message>
    <message>
        <source>very low battery notification</source>
        <translation type="vanished">电量非常低</translation>
    </message>
    <message>
        <source>battery is very low,please plug in!</source>
        <translation type="vanished">电量即将耗尽！</translation>
    </message>
    <message>
        <source>PowerManagement</source>
        <translation type="vanished">电源管理</translation>
    </message>
    <message>
        <source>Stats</source>
        <oldsource>Statistics</oldsource>
        <translation type="vanished">电源统计</translation>
    </message>
    <message>
        <source>PowerManager</source>
        <translation type="vanished">电源管理</translation>
    </message>
    <message>
        <source>PowerStatistics</source>
        <translation type="vanished">电源统计</translation>
    </message>
    <message>
        <source>PowerSaveMode</source>
        <translation type="vanished">省电模式</translation>
    </message>
    <message>
        <source>BatterySave</source>
        <translation type="vanished">电池养护</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="vanished">亮度</translation>
    </message>
</context>
<context>
    <name>PowerTray</name>
    <message>
        <location filename="../powertray.cpp" line="143"/>
        <source>SetPower</source>
        <translation type="unfinished">设置电源和睡眠</translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="enginedevice.cpp" line="583"/>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>powerwindow</name>
    <message>
        <location filename="../powerwindow.cpp" line="138"/>
        <location filename="../powerwindow.cpp" line="296"/>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="165"/>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="293"/>
        <source>Discharging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="299"/>
        <source>fully charged</source>
        <translation type="unfinished">已充满</translation>
    </message>
    <message>
        <source>PowerManagement</source>
        <translation type="obsolete">电源管理</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="157"/>
        <source>Endurance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="173"/>
        <source>PowerSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="176"/>
        <location filename="../powerwindow.cpp" line="273"/>
        <source>PowerMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="277"/>
        <source>BatteryMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="291"/>
        <source>Left %1h %2m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fullly charged notification</source>
        <translation type="obsolete">电池充满</translation>
    </message>
    <message>
        <source>low battery notification</source>
        <translation type="obsolete">电量低</translation>
    </message>
    <message>
        <source>critical battery notification</source>
        <translation type="obsolete">电量很低</translation>
    </message>
    <message>
        <source>battery is critical low,please plug in!</source>
        <translation type="obsolete">电量很低，请充电</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="402"/>
        <location filename="../powerwindow.cpp" line="419"/>
        <location filename="../powerwindow.cpp" line="455"/>
        <source>Better endurance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="406"/>
        <location filename="../powerwindow.cpp" line="423"/>
        <location filename="../powerwindow.cpp" line="451"/>
        <source>Better performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="410"/>
        <location filename="../powerwindow.cpp" line="427"/>
        <location filename="../powerwindow.cpp" line="447"/>
        <source>Best performance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
