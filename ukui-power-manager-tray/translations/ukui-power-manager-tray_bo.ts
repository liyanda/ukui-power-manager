<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo">
<context>
    <name>EngineDevice</name>
    <message>
        <location filename="../enginedevice.cpp" line="101"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="101"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="120"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="120"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="287"/>
        <source>Left %1h %2m (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="300"/>
        <source>charging (%1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="331"/>
        <source>AC adapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="281"/>
        <source>%1% available, charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="292"/>
        <source>%1% available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="298"/>
        <source>Left %1h %2m to full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="306"/>
        <source>%1 waiting to discharge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="311"/>
        <source>%1 waiting to charge (%2%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="335"/>
        <source>Laptop battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="339"/>
        <source>UPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="343"/>
        <source>Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="347"/>
        <source>Mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="351"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="355"/>
        <source>PDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="359"/>
        <source>Cell phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="363"/>
        <source>Media player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="367"/>
        <source>Tablet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="371"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../enginedevice.cpp" line="375"/>
        <source>unrecognised</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerTray</name>
    <message>
        <location filename="../powertray.cpp" line="143"/>
        <source>SetPower</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="enginedevice.cpp" line="826"/>
        <source></source>
        <comment>tablet device</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>powerwindow</name>
    <message>
        <location filename="../powerwindow.cpp" line="138"/>
        <location filename="../powerwindow.cpp" line="296"/>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="157"/>
        <source>Endurance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="165"/>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="173"/>
        <source>PowerSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="176"/>
        <location filename="../powerwindow.cpp" line="273"/>
        <source>PowerMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="277"/>
        <source>BatteryMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="291"/>
        <source>Left %1h %2m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="293"/>
        <source>Discharging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="299"/>
        <source>fully charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="402"/>
        <location filename="../powerwindow.cpp" line="419"/>
        <location filename="../powerwindow.cpp" line="455"/>
        <source>Better endurance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="406"/>
        <location filename="../powerwindow.cpp" line="423"/>
        <location filename="../powerwindow.cpp" line="451"/>
        <source>Better performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="410"/>
        <location filename="../powerwindow.cpp" line="427"/>
        <location filename="../powerwindow.cpp" line="447"/>
        <source>Best performance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
