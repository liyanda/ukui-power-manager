#ifndef BATTERYICON_H
#define BATTERYICON_H

#include <QWidget>
#include <QPushButton>
#include <QIcon>
#include <QGSettings>
#include <QtDBus>
#include <customstyle.h>

class BatteryIcon : public QPushButton
{
    Q_OBJECT
public:
    explicit BatteryIcon(QWidget *parent = nullptr);

private:
    void gsettingWacther();

    QDBusInterface   *m_iface;

protected:
//    void paintEvent(QPaintEvent *);
private Q_SLOTS:
    void iconChange(QString);
Q_SIGNALS:

};

#endif // BATTERYICON_H
