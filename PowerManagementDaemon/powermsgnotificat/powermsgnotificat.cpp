/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "powermsgnotificat.h"

PowerMsgNotificat::PowerMsgNotificat() {}

PowerMsgNotificat::~PowerMsgNotificat() {}

void PowerMsgNotificat::initPowerMsgNotificat()
{
    QDBusConnection::sessionBus().connect(
        QString(""),
        QString("/"),
        QString("org.ukui.upower"),
        QString("PowerState"),
        this,
        SLOT(msgNotification(bool)));
    QDBusConnection::systemBus().connect(
        UPOWER_SERVICE, UPOWER_PATH, FREEDESKTOP_UPOWER, "PropertiesChanged", this, SLOT(dealBatteryStateMssage(void)));
    m_batteryState = getBateeryState();
    m_isFullNotification = false;
}

void PowerMsgNotificat::msgNotification(bool isPowerState)
{
    qDebug() << "PowerMsgNotificat::msgNotification" << isPowerState;
    QString mType,msg;
    if (!isPowerState) {
        mType = tr("charge notification");
        msg = tr("battery is charging");
        notifySend(mType, msg);
    } else {
        mType = tr("discharged notification");
        msg = tr("battery is discharging");
        notifySend(mType, msg);
    }

}

void PowerMsgNotificat::dealBatteryStateMssage(void)
{
    qDebug() << "PowerMsgNotificat::dealBatteryStateMssage";
    m_batteryState = getBateeryState();
    qDebug() << "m_batState" << m_batteryState;
    if (4 == m_batteryState && !m_isFullNotification){
        QString mType = tr("full charge notification");
        QString msg = tr("battery is full");
        notifySend(mType, msg);
        m_isFullNotification = true;
    } else {
        m_isFullNotification = false;
    }
}

int PowerMsgNotificat::getBateeryState()
{
    int state = -1;
    QDBusInterface iface("org.ukui.upower","/","org.ukui.upower",QDBusConnection::sessionBus());
    QDBusReply<int> reply = iface.call("BatteryState");
    if (reply.isValid()) {
        state = reply.value();
    } else {
        qDebug() << "State error!";
    }
    return state;
}

void PowerMsgNotificat::notifySend(const QString &type, const QString &arg)
{
    QDBusInterface iface(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications",
        QDBusConnection::sessionBus());
    QList<QVariant> args;
    QStringList argg;
    QMap<QString, QVariant> pear_map;
    args << tr("Power Manager") << ((unsigned int)0) << QString("ukui-power-manager")
         << type //显示的是什么类型的信息//系统升级
         << arg  //显示的具体信息
         << argg << pear_map << (int)-1;
    QDBusReply<uint> reply = iface.callWithArgumentList(QDBus::AutoDetect, "Notify", args);
    qDebug() << "msg code is ：" << reply.value();
}
