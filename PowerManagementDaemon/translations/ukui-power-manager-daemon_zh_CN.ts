<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>LowPowerWatcher</name>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="130"/>
        <source>Low battery notification</source>
        <translation>低电量消息通知</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="131"/>
        <source>The system enters a low battery state</source>
        <translation>系统进入低电量状态</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="139"/>
        <source>Very low battery notification</source>
        <translation>极低电量消息通知</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="143"/>
        <source>Current power is：%1%. The system will turn off the display in one minute</source>
        <translation>当前电量为：%1，系统将于一分钟后关闭显示器</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="147"/>
        <source>Current power is：%1%，The system will suspend in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后睡眠</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="150"/>
        <source>Current power is：%1%，The system will shutdown in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后关机</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="153"/>
        <source>Current power is：%1%，The system will hibernate in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后关机</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="191"/>
        <source>Power Manager</source>
        <translation>电源管理器</translation>
    </message>
</context>
<context>
    <name>PowerMsgNotificat</name>
    <message>
        <source>error message</source>
        <translation type="vanished">错误消息</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="45"/>
        <source>charge notification</source>
        <translation>充电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="46"/>
        <source>battery is charging</source>
        <translation>电池正在充电</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="49"/>
        <source>discharged notification</source>
        <translation>放电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="50"/>
        <source>battery is discharging</source>
        <translation>电池正在放电中</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="62"/>
        <source>full charge notification</source>
        <translation>充满电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="63"/>
        <source>battery is full</source>
        <translation>电池现在已经充满</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="94"/>
        <source>Power Manager</source>
        <translation>电源管理器</translation>
    </message>
</context>
</TS>
