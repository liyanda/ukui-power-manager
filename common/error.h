/*
 * Copyright 2021 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define ERROR_H

//Qt
#include <QDebug>

#define SUCCESS                                     0

#define INIT_CONFIG_ERROR                           100
#define CONFIG_QSET_INIT_ERROR                      101
#define CONFIG_PRODUCT_NAME_ERROR                   102
#define CONFIG_VENDOR_GET_ERROR                     103
#define CONFIG_GET_CONF_DATA_ERROR                  104
#define CONFIG_GET_MACHINE_TYPE_ERROR               105

#define INIT_BRIGHTNESS_ERROR                       200

#define INIT_LID_ERROR                              300
#define LID_GET_INHIBIT_ERROR                       301
#define LID_CREATE_LOGIN1_CONNECTION_ERROR          302
#define LID_CREATE_UPOWER_CONNECTION_ERROR          303

#define INIT_POWER_MANAGER_CONTROL_ERROR            400

#define INIT_IDLE_ERROR                             500
#define IDLE_SESSION_CONNECT_ERROR                  501

#define INIT_LOW_BATTERY_ERROR                      600
#define GET_BATTERY_DATA_ERROR                      601

#define INIT_POWER_POLICY_ERROR                     700
#define GET_CPU_MODE_ERROR                          701

#define POWER_STATE_CREATE_CONNECTION_ERROR         800
#define BATTERY_STATE_CREATE_CONNECTION_ERROR       801

#endif // ERROR_H
